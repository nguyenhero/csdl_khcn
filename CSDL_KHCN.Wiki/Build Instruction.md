# FE
## requirement
 - NodeJs : https://nodejs.org/en/
 - Yarn : https://yarnpkg.com/lang/en/
 - npm i -g http-server
 - yarn install
## Dev
- cd CSDL_KHCN.FE
- http-server -p 5002
## Build
- cd CSDL_KHCN.FE
- npm run build
# API
## requirement
 - Dotnet Core : https://www.microsoft.com/net/download
## Dev
- cd CSDL_KHCN.API
- dotnet run 
## Build 
- cd CSDL_KHCN.API
- dotnet build 
