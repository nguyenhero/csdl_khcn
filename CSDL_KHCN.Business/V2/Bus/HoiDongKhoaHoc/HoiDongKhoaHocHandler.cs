﻿using LinqKit;
using CSDL_KHCN.Data_V2;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Serilog;
using System.Linq;
namespace CSDL_KHCN.Business_V2
{
    public class HoiDongKhoaHocHandler : IHoiDongKhoaHocHandler
    {
        private readonly DbHandler<BusHoiDongKhoaHoc, HoiDongKhoaHocModel, HoiDongKhoaHocQueryModel> _dbHandler = DbHandler<BusHoiDongKhoaHoc, HoiDongKhoaHocModel, HoiDongKhoaHocQueryModel>.Instance;

        #region CRUD
        public async Task<Response> FindAsync(Guid id)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var data = await unitOfWork.GetRepository<BusHoiDongKhoaHoc>().FindAsync(id);
                    var dataItem = (from tv in unitOfWork.GetRepository<BusThanhVienHoiDongKhoaHoc>().GetAll()
                                    join nkh in unitOfWork.GetRepository<BusNhaKhoaHoc>().GetAll()
                                    on tv.NhaKhoaHocId equals nkh.Id
                                    where tv.HoiDongKhoaHocId == id
                                    select new
                                    {
                                        ThanhVien = tv,
                                        NhaKhoaHoc = nkh
                                    }).ToList();
                    var result = AutoMapperUtils.AutoMap<BusHoiDongKhoaHoc, HoiDongKhoaHocModel>(data);
                    var listTV = dataItem.Select(s => s.ThanhVien).ToList();
                    var listNKH = dataItem.Select(s => s.NhaKhoaHoc).ToList();
                    var listTVModel = AutoMapperUtils.AutoMap<BusThanhVienHoiDongKhoaHoc, ThanhVienHoiDongKhoaHocModel>(listTV);
                    var listNKHModel = AutoMapperUtils.AutoMap<BusNhaKhoaHoc, NhaKhoaHocModel>(listNKH);
                    foreach (var tv in listTVModel)
                    {
                        tv.NhaKhoaHoc = listNKHModel.Find(s => s.Id == tv.NhaKhoaHocId);
                    }
                    result.ThanhVienHoiDongKhoaHocs = listTVModel;
                    return new ResponseObject<HoiDongKhoaHocModel>(result);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }


        }
        public async Task<Response> CreateAsync(HoiDongKhoaHocCreateModel model, Guid applicationId, Guid userId)
        {

            BusHoiDongKhoaHoc request = AutoMapperUtils.AutoMap<HoiDongKhoaHocCreateModel, BusHoiDongKhoaHoc>(model);
            request = request.InitCreate(applicationId, userId);
            request.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(request);
            if (result.Code == Code.Success)
            {
                var resultSub = await ChangeSubItem(model.ThanhVienHoiDongKhoaHocs.ToList(), request.Id);
                if (resultSub.Code == Code.Success)
                {
                    return result;
                }
                return resultSub;
            }



            return result;
        }
        public async Task<Response> UpdateAsync(Guid id, HoiDongKhoaHocUpdateModel model, Guid applicationId, Guid userId)
        {
            BusHoiDongKhoaHoc request = AutoMapperUtils.AutoMap<HoiDongKhoaHocUpdateModel, BusHoiDongKhoaHoc>(model);
            request = request.InitUpdate(applicationId, userId);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request);
            if (result.Code == Code.Success)
            {
                var resultSub = await ChangeSubItem(model.ThanhVienHoiDongKhoaHocs.ToList(), request.Id);
                if (resultSub.Code == Code.Success)
                {
                    return result;
                }
                return resultSub;
            }
            return result;
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            return result;
        }



        public async Task<Response> ChangeSubItem(List<ThanhVienHoiDongKhoaHocModel> listItem, Guid id)
        {

            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //Delete current
                    unitOfWork.GetRepository<BusThanhVienHoiDongKhoaHoc>().DeleteRange(s => s.HoiDongKhoaHocId == id);
                    var listRequest = AutoMapperUtils.AutoMap<ThanhVienHoiDongKhoaHocModel, BusThanhVienHoiDongKhoaHoc>(listItem);
                    foreach (var item in listRequest)
                    {
                        item.HoiDongKhoaHocId = id;
                        item.NhaKhoaHoc = null;
                    }
                    unitOfWork.GetRepository<BusThanhVienHoiDongKhoaHoc>().AddRange(listRequest);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "");

                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }


        }


        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<BusHoiDongKhoaHoc>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(HoiDongKhoaHocQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetPageAsync(HoiDongKhoaHocQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<BusHoiDongKhoaHoc, bool>> BuildQuery(HoiDongKhoaHocQueryModel query)
        {
            var predicate = PredicateBuilder.New<BusHoiDongKhoaHoc>(true);

            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s => s.Ten.Contains(query.FullTextSearch));
            }


            if (query.TrangThai.HasValue)
            {
                predicate.And(s => s.TrangThai == query.TrangThai.Value);
            }
            if (query.NamThanhLap.HasValue)
            {
                predicate.And(s => s.NamThanhLap == query.NamThanhLap.Value);
            }
            return predicate;
        }
    }
}
