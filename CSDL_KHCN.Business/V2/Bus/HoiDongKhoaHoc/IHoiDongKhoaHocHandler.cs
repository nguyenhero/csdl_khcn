﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CSDL_KHCN.Data_V2;

namespace CSDL_KHCN.Business_V2
{
    public interface IHoiDongKhoaHocHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(HoiDongKhoaHocCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, HoiDongKhoaHocUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(HoiDongKhoaHocQueryModel query);
        Task<Response> GetPageAsync(HoiDongKhoaHocQueryModel query);
        Response GetAll();
    }
}