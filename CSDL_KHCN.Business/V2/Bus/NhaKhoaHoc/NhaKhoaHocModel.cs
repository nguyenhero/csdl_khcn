﻿using CSDL_KHCN.Data_V2;
using System;
namespace CSDL_KHCN.Business_V2
{
    public class BaseNhaKhoaHocModel : BaseModel
    {
        public Guid Id { get; set; }
    }
    public class NhaKhoaHocModel : BaseNhaKhoaHocModel
    {
        public string HoTen { get; set; }
        public int NamSinh { get; set; }
        public int GioiTinh { get; set; }
        public string HocHam { get; set; }
        public string HocVi { get; set; }
        public string LinhVuc { get; set; }
        public string ChucDanhNghienCuu { get; set; }
        public string ChucVuHienNay { get; set; }
        public string DienThoaiNhaRieng { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string TenCoQuan { get; set; }
        public string TenNguoiDungDauCoQuan { get; set; }
        public string DiaChiCoQuan { get; set; }
        public string DienThoaiCoQuan { get; set; }
        public string FaxCoQuan { get; set; }
        public string WebsiteCoQuan { get; set; }
        public string FileDinhKem { get; set; }
    }
    public class NhaKhoaHocQueryModel : PaginationRequest
    {
        public string HoTen { get; set; }
        public string LinhVuc { get; set; }
        public string ChucDanhNghienCuu { get; set; }
    }

    public class NhaKhoaHocCreateModel
    {
        public string HoTen { get; set; }
        public int NamSinh { get; set; }
        public int GioiTinh { get; set; }
        public string HocHam { get; set; }
        public string HocVi { get; set; }
        public string LinhVuc { get; set; }
        public string ChucDanhNghienCuu { get; set; }
        public string ChucVuHienNay { get; set; }
        public string DienThoaiNhaRieng { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string TenCoQuan { get; set; }
        public string TenNguoiDungDauCoQuan { get; set; }
        public string DiaChiCoQuan { get; set; }
        public string DienThoaiCoQuan { get; set; }
        public string FaxCoQuan { get; set; }
        public string WebsiteCoQuan { get; set; }
        public string FileDinhKem { get; set; }
    }
    public class NhaKhoaHocUpdateModel
    {
        public string HoTen { get; set; }
        public int NamSinh { get; set; }
        public int GioiTinh { get; set; }
        public string HocHam { get; set; }
        public string HocVi { get; set; }
        public string LinhVuc { get; set; }
        public string ChucDanhNghienCuu { get; set; }
        public string ChucVuHienNay { get; set; }
        public string DienThoaiNhaRieng { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string TenCoQuan { get; set; }
        public string TenNguoiDungDauCoQuan { get; set; }
        public string DiaChiCoQuan { get; set; }
        public string DienThoaiCoQuan { get; set; }
        public string FaxCoQuan { get; set; }
        public string WebsiteCoQuan { get; set; }
        public string FileDinhKem { get; set; }
    }
}

