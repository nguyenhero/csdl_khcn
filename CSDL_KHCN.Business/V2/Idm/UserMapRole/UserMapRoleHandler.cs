﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSDL_KHCN.Data_V2;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace CSDL_KHCN.Business_V2
{
    public class UserMapRoleHandler : IUserMapRoleHandler
    {
        public async Task<Response> DeleteUserMapRoleAsync(Guid roleId, Guid userId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var currentMap = await unitOfWork.GetRepository<IdmUserMapRole>().FindAsync(s =>
                        s.RoleId == roleId && s.UserId == userId && s.ApplicationId == applicationId);
                    if (currentMap == null)
                        return new ResponseError(Code.BadRequest, "Người dùng không thuộc nhóm người dùng");

                    #endregion
                    unitOfWork.GetRepository<IdmUserMapRole>().Delete(currentMap);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Loại người dùng ra khỏi nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> DeleteUserMapRoleAsync(Guid roleId, IList<Guid> listUserId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrenMap = await unitOfWork.GetRepository<IdmUserMapRole>().GetListAsync(s =>
                        s.RoleId == roleId && listUserId.Contains(s.UserId) && s.ApplicationId == applicationId);
                    if (listCurrenMap.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không người dùng nào thuộc nhóm người dùng");

                    #endregion
                    //Delete Mapp
                    unitOfWork.GetRepository<IdmUserMapRole>().DeleteRange(listCurrenMap);

                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Loại danh sách người dùng ra khỏi nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> DeleteUserMapRoleAsync(IList<Guid> listRoleId, Guid userId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrenMap = await unitOfWork.GetRepository<IdmUserMapRole>().GetListAsync(s =>
                        listRoleId.Contains(s.RoleId) && userId == s.UserId && s.ApplicationId == applicationId);
                    if (listCurrenMap.Count == 0)
                        return new ResponseError(Code.BadRequest, "Người dùng không thuộc nhóm người dùng nào");

                    #endregion

                    //Delete Mapp
                    unitOfWork.GetRepository<IdmUserMapRole>().DeleteRange(listCurrenMap);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Loại người dùng ra khỏi danh sách nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> AddUserMapRoleAsync(Guid roleId, Guid userId, Guid applicationId, Guid? app,
            Guid? actor)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var currentMap = await unitOfWork.GetRepository<IdmUserMapRole>().FindAsync(s =>
                        s.RoleId == roleId && s.UserId == userId && s.ApplicationId == applicationId);
                    if (currentMap != null)
                        return new ResponseError(Code.BadRequest, "Người dùng đã tồn tại trong nhóm");

                    var roleModel = RoleCollection.Instance.GetModel(roleId);
                    if (roleModel == null)
                        return new ResponseError(Code.BadRequest, "Nhóm người dùng không tồn tại");
                    var userModel = UserCollection.Instance.GetModel(userId);
                    if (userModel == null)
                        return new ResponseError(Code.BadRequest, "Người dùng không tồn tại");

                    #endregion

                    unitOfWork.GetRepository<IdmUserMapRole>().Add(new IdmUserMapRole
                    {
                        UserId = userId,
                        RoleId = roleId,
                        ApplicationId = applicationId
                    }.InitCreate(applicationId, actor));
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Gán người dùng vào nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> AddUserMapRoleAsync(Guid roleId, IList<Guid> listUserId, Guid applicationId,
            Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrentMap = await unitOfWork.GetRepository<IdmUserMapRole>().GetListAsync(s =>
                        s.RoleId == roleId && listUserId.Contains(s.UserId) && s.ApplicationId == applicationId);
                    if (listCurrentMap.Count == listUserId.Count)
                        return new ResponseError(Code.BadRequest, "Tất cả người dùng đã tồn tại trong nhóm !");


                    var roleModel = RoleCollection.Instance.GetModel(roleId);
                    if (roleModel == null)
                        return new ResponseError(Code.BadRequest, "Nhóm người dùng không tồn tại");
                    var listUserModel = UserCollection.Instance.GetModel(s => listUserId.Contains(s.Id));
                    if (listUserModel == null || listUserModel.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không người dùng nào tồn tại");

                    #endregion

                    var listUmrToAdd = new List<IdmUserMapRole>();
                    foreach (var userModel in listUserModel)
                        listUmrToAdd.Add(new IdmUserMapRole
                        {
                            UserId = userModel.Id,
                            RoleId = roleId,
                            ApplicationId = applicationId
                        }.InitCreate(applicationId, actorId));
                    if (listUmrToAdd.Any()) unitOfWork.GetRepository<IdmUserMapRole>().AddRange(listUmrToAdd);

                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Thêm danh sách người dùng vào nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> AddUserMapRoleAsync(IList<Guid> listRoleId, Guid userId, Guid applicationId,
            Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrentMap = await unitOfWork.GetRepository<IdmUserMapRole>().GetListAsync(s =>
                        s.UserId == userId && listRoleId.Contains(s.RoleId) && s.ApplicationId == applicationId);
                    if (listCurrentMap.Count == listRoleId.Count)
                        return new ResponseError(Code.BadRequest, "Người dùng đã tồn tại trong tất cả nhóm !");


                    var userModel = UserCollection.Instance.GetModel(userId);
                    if (userModel == null)
                        return new ResponseError(Code.BadRequest, "Người dùng không tồn tại");
                    var listRoleModel = RoleCollection.Instance.Collection.Where(s => listRoleId.Contains(s.Id))
                        .ToList();
                    if (listRoleModel.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không nhóm người dùng nào tồn tại");

                    #endregion

                    var listUmrToAdd = new List<IdmUserMapRole>();
                    foreach (var roleModel in listRoleModel)
                        listUmrToAdd.Add(new IdmUserMapRole
                        {
                            UserId = userId,
                            RoleId = roleModel.Id,
                            ApplicationId = applicationId
                        }.InitCreate(applicationId, actorId));
                    if (listUmrToAdd.Any()) unitOfWork.GetRepository<IdmUserMapRole>().AddRange(listUmrToAdd);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Gán người dùng vào danh sách nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetUserMapRoleAsync(Guid roleId, Guid applicationId)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var listUserId = await (from rmu in unitOfWork.GetRepository<IdmUserMapRole>().GetAll()
                                            where rmu.RoleId == roleId && rmu.ApplicationId == applicationId
                                            select rmu.UserId).ToListAsync();
                    var resultData = UserCollection.Instance.GetModel(s => listUserId.Contains(s.Id)).ToList();
                    return new ResponseObject<IList<BaseUserModel>>(resultData);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetRoleMapUserAsync(Guid userId, Guid applicationId)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = from rmu in unitOfWork.GetRepository<IdmUserMapRole>().GetAll()
                                join r in unitOfWork.GetRepository<IdmRole>().GetAll()
                                    on rmu.RoleId equals r.Id
                                where rmu.UserId == userId && rmu.ApplicationId == applicationId
                                select r;
                    var queryList = await datas.ToListAsync();
                    var resultData = AutoMapperUtils.AutoMap<IdmRole, BaseRoleModel>(queryList);
                    return new ResponseObject<IList<BaseRoleModel>>(resultData);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> CheckRightMapUserAsync(Guid userId, Guid roleId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var currentMap = await unitOfWork.GetRepository<IdmUserMapRole>().FindAsync(s =>
                        s.UserId == userId && s.RoleId == roleId && s.ApplicationId == applicationId);
                    if (currentMap != null)
                        return new ResponseObject<bool>(true);
                    return new ResponseObject<bool>(false);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
    }
}