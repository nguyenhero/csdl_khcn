﻿using CSDL_KHCN.Data_V2;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CSDL_KHCN.Business_V2
{
    public interface IUserHandler
    {

        #region CRUD 
        Task<Response> CreateAsync(UserCreateModel model, Guid applicationId);
        Task<Response> UpdateAsync(Guid id, UserUpdateModel model, Guid applicationId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> FindAsync(Guid id);
        #endregion

        Task<Response> CheckNameAvailability(string name,  Guid? applicationId = null);
        Task<Response> GetDetail(Guid id, Guid applicationId);
        Task<Response> GetPageAsync(UserQueryModel query);
        Task<Response> GetAllAsync(UserQueryModel query);
        Task<Response> GetAllAsync();
        Response GetAll();
        Response Authencate(string userName, string password);
        Task<Response> ChangePasswordAsync(Guid id, string password, Guid applicationId);
    }
}
