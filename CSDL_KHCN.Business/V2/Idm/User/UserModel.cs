﻿using CSDL_KHCN.Data_V2;
using System;
using System.Collections.Generic;

namespace CSDL_KHCN.Business_V2
{
    public class BaseUserModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
    }
    public class UserModel : BaseUserModel
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string AvatarUrl { get; set; }
        public int? Type { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public DateTime? Birthdate { get; set; }
        public DateTime LastActivityDate { get; set; }
    }
    public class UserDetailModel : UserModel
    {
        public IList<BaseRoleModel> ListRole { get; set; }
    }
    public class UserQueryModel : PaginationRequest
    {
        public string UserName { get; set; }
    }

    public class UserCreateModel
    {
        /// <summary>
        /// preset
        /// </summary>
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string AvatarUrl { get; set; }
        public int? Type { get; set; }
        public string Password { get; set; } 
        public DateTime? Birthdate { get; set; } 
        //Plus
        /// <summary>
        /// Danh sách quyền thêm
        /// </summary>
        public IList<Guid> ListAddRightId { get; set; }
        /// <summary>
        /// Danh sách nhóm người dùng thêm
        /// </summary>
        public IList<Guid> ListAddRoleId { get; set; }
    }
    public class UserUpdateModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string AvatarUrl { get; set; }
        public int? Type { get; set; }
        public string Password { get; set; }
        public DateTime? Birthdate { get; set; }
        //Plus
        /// <summary>
        /// Danh sách quyền thêm
        /// </summary>
        public IList<Guid> ListAddRightId { get; set; }
        /// <summary>
        /// Danh sách nhóm người dùng thêm
        /// </summary>
        public IList<Guid> ListAddRoleId { get; set; }
        //Plus
        /// <summary>
        /// Danh sách quyền xóa
        /// </summary>
        public IList<Guid> ListDeleteRightId { get; set; }
        /// <summary>
        /// Danh sách nhóm người dùng xóa
        /// </summary>
        public IList<Guid> ListDeleteRoleId { get; set; }
    }
}

