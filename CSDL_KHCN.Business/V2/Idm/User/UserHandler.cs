﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LinqKit;
using CSDL_KHCN.Data_V2;
using Serilog;

namespace CSDL_KHCN.Business_V2
{
    public class UserHandler : IUserHandler
    {
        private readonly DbHandler<IdmUser, UserModel, UserQueryModel> _dbHandler =
            DbHandler<IdmUser, UserModel, UserQueryModel>.Instance;

        private readonly IUserMapRoleHandler _userMapRoleHandler;

        public UserHandler(IUserMapRoleHandler userMapRoleHandler)
        {
            _userMapRoleHandler = userMapRoleHandler;
        }

        public UserHandler()
        {
        }

        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> CheckNameAvailability(string name, Guid? applicationId = null)
        {

            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var result = await unitOfWork.GetRepository<IdmUser>().FindAsync(s => s.Name == name);
                    return new ResponseObject<bool>(result == null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetDetail(Guid id, Guid applicationId)
        {
            var model = await _dbHandler.FindAsync(id);
            if (model.Code == Code.Success)
            {
                var modelData = model as ResponseObject<UserModel>;
                var result = AutoMapperUtils.AutoMap<UserModel, UserDetailModel>(modelData?.Data);
                var listRoleResponse = await _userMapRoleHandler.GetRoleMapUserAsync(id, applicationId);
                var listRoleResponseData = listRoleResponse as ResponseObject<IList<BaseRoleModel>>;
                if (listRoleResponseData != null) result.ListRole = listRoleResponseData.Data;
                return new ResponseObject<UserDetailModel>(result);
            }

            return model;
        }

        public Task<Response> GetPageAsync(UserQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }

        public Task<Response> GetAllAsync(UserQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate);
        }

        public Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<IdmUser>(true);
            return _dbHandler.GetAllAsync(predicate);
        }

        public Response Authencate(string userName, string password)
        {
            var userCollection = UserCollection.Instance.Collection;
            foreach (var user in userCollection)
                if (user.UserName == userName)
                {
                    var passhash = Utils.PasswordGenerateHmac(password, user.PasswordSalt);
                    if (passhash == user.Password) return new ResponseObject<UserModel>(user);
                    return new ResponseError(Code.BadRequest, "Sai mật khẩu");
                }

            return new ResponseError(Code.BadRequest, "Không tìm thấy tài khoản");
        }

        private Expression<Func<IdmUser, bool>> BuildQuery(UserQueryModel query)
        {
            var predicate = PredicateBuilder.New<IdmUser>(true);

            if (!string.IsNullOrEmpty(query.UserName)) predicate.And(s => s.UserName == query.UserName);
            if (query.Id.HasValue) predicate.And(s => s.Id == query.Id);
            if (query.ListId != null) predicate.And(s => query.ListId.Contains(s.Id));

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(s => s.UserName.Contains(query.FullTextSearch));
            return predicate;
        }

        #region CRUD 

        public async Task<Response> CreateAsync(UserCreateModel model, Guid applicationId)
        {
            //Check trùng
            var userCheck = UserCollection.Instance.CheckName(model.UserName);
            if (userCheck) return new ResponseError(Code.BadRequest, "Tên người dùng đã tồn tại");
            var request = AutoMapperUtils.AutoMap<UserCreateModel, IdmUser>(model);
            request.Id = Guid.NewGuid();
            //Cập nhật pass
            Log.Fatal(request.Password);

            Log.Fatal(request.PasswordSalt);
            if (!string.IsNullOrEmpty(request.Password))
            {
                request.PasswordSalt = Utils.PassowrdCreateSalt512();
                request.Password = Utils.PasswordGenerateHmac(request.Password, request.PasswordSalt);

                Log.Fatal(request.Password);

                Log.Fatal(request.PasswordSalt);
            }

            var result = await _dbHandler.CreateAsync(request);
            if (result.Code == Code.Success)
            {
                UserCollection.Instance.LoadToHashSet();

                #region Realtive
                if (model.ListAddRoleId != null)
                    await _userMapRoleHandler.AddUserMapRoleAsync(model.ListAddRoleId, request.Id, applicationId,
                        request.Id, applicationId);

                #endregion
            }

            return result;
        }

        public async Task<Response> UpdateAsync(Guid id, UserUpdateModel model, Guid applicationId)
        {
            //Check trùng
            var userCheck = UserCollection.Instance.CheckOtherUser(model.UserName, id);
            if (userCheck) return new ResponseError(Code.BadRequest, "Tên người dùng đã tồn tại");
            var request = AutoMapperUtils.AutoMap<UserUpdateModel, IdmUser>(model);
            //Cập nhật pass
            if (!string.IsNullOrEmpty(request.Password))
            {
                request.PasswordSalt = Utils.PassowrdCreateSalt512();
                request.Password = Utils.PasswordGenerateHmac(request.Password, request.PasswordSalt);
            }

            var result = await _dbHandler.UpdateAsync(id, request);
            if (result.Code == Code.Success)
            {
                UserCollection.Instance.LoadToHashSet();

                #region Realtive


                if (model.ListAddRoleId != null)
                    await _userMapRoleHandler.AddUserMapRoleAsync(model.ListAddRoleId, id, applicationId, applicationId,
                        id);
                if (model.ListDeleteRoleId != null)
                    await _userMapRoleHandler.DeleteUserMapRoleAsync(model.ListAddRoleId, id, applicationId);

                #endregion
            }

            return result;
        }

        public async Task<Response> DeleteAsync(Guid id)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    /// Delete user map role
                    unitOfWork.GetRepository<IdmUserMapRole>().DeleteRange(s => s.UserId == id);


                    await unitOfWork.SaveAsync();
                }
                var result = await _dbHandler.DeleteAsync(id);
                UserCollection.Instance.LoadToHashSet();
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }



        public async Task<Response> ChangePasswordAsync(Guid id, string password, Guid applicationId)
        {
            var currentUser = UserCollection.Instance.Collection.Where(s => s.Id == id).FirstOrDefault();
            if (currentUser != null)
            {
                currentUser.PasswordSalt = Utils.PassowrdCreateSalt512();
                currentUser.Password = Utils.PasswordGenerateHmac(password, currentUser.PasswordSalt);
            }
            var request = AutoMapperUtils.AutoMap<UserModel, IdmUser>(currentUser);

            var result = await _dbHandler.UpdateAsync(id, request);
            if (result.Code == Code.Success)
            {
                UserCollection.Instance.LoadToHashSet();
            }

            return result;
        }

        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    /// Delete user map role
                    unitOfWork.GetRepository<IdmUserMapRole>().DeleteRange(s => listId.Contains(s.UserId));


                    await unitOfWork.SaveAsync();
                }
                var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
                UserCollection.Instance.LoadToHashSet();
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }

        }

        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }

        #endregion
    }
}