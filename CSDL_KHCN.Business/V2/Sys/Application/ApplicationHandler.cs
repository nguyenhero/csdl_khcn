﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LinqKit;
using CSDL_KHCN.Data_V2;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace CSDL_KHCN.Business_V2
{
    public class ApplicationHandler : IApplicationHandler
    {
        private readonly DbHandler<SysApplication, ApplicationModel, ApplicationQueryModel> _dbHandler =
            DbHandler<SysApplication, ApplicationModel, ApplicationQueryModel>.Instance;



        public ApplicationHandler()
        {
        }

        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }

        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<SysApplication>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }

        public Task<Response> GetAllAsync(ApplicationQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate);
        }

        public Task<Response> GetPageAsync(ApplicationQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }

        public async Task<Response> BootstrapProjectDataAsync(int numberOfLoop = 2)
        {
            try
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                using (var unitOfWork = new UnitOfWork())
                {
                    await unitOfWork.MigrateAsync();
                }
                return new ResponseUpdate(Guid.Empty);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        private Expression<Func<SysApplication, bool>> BuildQuery(ApplicationQueryModel query)
        {
            var predicate = PredicateBuilder.New<SysApplication>(true);
            if (!string.IsNullOrEmpty(query.Name)) predicate.And(s => s.Name == query.Name);
            if (!string.IsNullOrEmpty(query.Name)) predicate.And(s => s.Name == query.Name);
            if (query.ListId != null && query.ListId.Count > 0) predicate.And(s => query.ListId.Contains(s.Id));
            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(s =>
                    s.Name.Contains(query.FullTextSearch) || s.Name.Contains(query.FullTextSearch) ||
                    s.Description.Contains(query.FullTextSearch));
            return predicate;
        }

        #region CRUD

        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }

        public async Task<Response> CreateAsync(ApplicationCreateModel model)
        {
            var request = new SysApplication
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                Code = model.Code ?? model.Name.ToUniqueName(),
                Description = model.Description
            };
            var result = await _dbHandler.CreateAsync(request);
            ApplicationCollection.Instance.LoadToHashSet();
            return result;
        }

        public async Task<Response> UpdateAsync(Guid id, ApplicationUpdateModel model)
        {
            var request = new SysApplication
            {
                Id = id,
                Name = model.Name,
                Code = model.Code ?? model.Name.ToUniqueName(),
                Description = model.Description
            };
            var result = await _dbHandler.UpdateAsync(id, request);
            ApplicationCollection.Instance.LoadToHashSet();
            return result;
        }

        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            ApplicationCollection.Instance.LoadToHashSet();
            return result;
        }

        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            ApplicationCollection.Instance.LoadToHashSet();
            return result;
        }

        public async Task<Response> GetAllByUserIdAsync(Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    var result = await (from a in unitOfWork.GetRepository<SysApplication>().GetAll()
                                        select a).Distinct().ToListAsync();
                    if (result != null)
                        return new ResponseObject<List<ApplicationModel>>(
                            AutoMapperUtils.AutoMap<SysApplication, ApplicationModel>(result));
                    return new ResponseError(Code.NotFound, "Không tìm thấy dữ liệu");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetDefautByUserIdAsync(Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    var result = await (from a in unitOfWork.GetRepository<SysApplication>().GetAll()

                                        select a).Distinct().FirstOrDefaultAsync();
                    if (result != null)
                        return new ResponseObject<ApplicationModel>(
                            AutoMapperUtils.AutoMap<SysApplication, ApplicationModel>(result));
                    return new ResponseError(Code.NotFound, "Không tìm thấy dữ liệu");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        #endregion
    }
}