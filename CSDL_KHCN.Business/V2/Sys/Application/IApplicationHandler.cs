﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CSDL_KHCN.Data_V2;

namespace CSDL_KHCN.Business_V2
{
    public interface IApplicationHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(ApplicationCreateModel model);
        Task<Response> UpdateAsync(Guid id, ApplicationUpdateModel model);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(ApplicationQueryModel query);
        Task<Response> GetPageAsync(ApplicationQueryModel query);
        Response GetAll();
        Task<Response> BootstrapProjectDataAsync(int option);
        Task<Response> GetAllByUserIdAsync(Guid userId);
        Task<Response> GetDefautByUserIdAsync(Guid userId);
        
    }
}