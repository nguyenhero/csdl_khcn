﻿using System;
using System.Collections.Generic;
using System.Linq;
using CSDL_KHCN.Data_V2;

namespace CSDL_KHCN.Business_V2
{
    public class ApplicationCollection
    {
        private readonly IApplicationHandler _handler;
        public HashSet<ApplicationModel> Collection;

        protected ApplicationCollection()
        {
            _handler = new ApplicationHandler();
            LoadToHashSet();
        }

        public static ApplicationCollection Instance { get; } = new ApplicationCollection();

        public void LoadToHashSet()
        {
            Collection = new HashSet<ApplicationModel>();
            var listResponse = _handler.GetAll();
            if (listResponse.Code == Code.Success)
            {
                // Add to hashset
                if (listResponse is ResponseObject<List<ApplicationModel>> listResponseObj)
                    foreach (var response in listResponseObj.Data)
                        Collection.Add(response);
            }
        }

        public string GetName(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result?.Name;
        }

        public BaseApplicationModel GetModel(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result;
        }
    }
}