﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CSDL_KHCN.Data_V2;

namespace CSDL_KHCN.Business_V2
{
    public interface IParameterHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(ParameterCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, ParameterUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(ParameterQueryModel query);
        Task<Response> GetPageAsync(ParameterQueryModel query);
        Response GetAll();
    }
}