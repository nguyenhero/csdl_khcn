﻿using System;
using System.Collections.Generic;
using System.Linq;
using CSDL_KHCN.Data_V2;

namespace CSDL_KHCN.Business_V2
{
    public class ParameterCollection
    {

        private readonly IParameterHandler _handler;
        public HashSet<ParameterModel> Collection;
        public static ParameterCollection Instance { get; } = new ParameterCollection();
        protected ParameterCollection()
        {
            _handler = new ParameterHandler();
            if (Collection == null || Collection.Count() == 0)
            {
                LoadToHashSet();
            }
        }
        public void LoadToHashSet()
        {
            Collection = new HashSet<ParameterModel>();
            var listResponse = _handler.GetAll();
            if (listResponse.Code == Code.Success)
            {
                // Add to hashset
                if (listResponse is ResponseObject<List<ParameterModel>> listResponseObj)
                    foreach (var response in listResponseObj.Data)
                    {
                        Collection.Add(response);
                    }
            }

        }

        public string GetValue(string name)
        {

            var result = Collection.FirstOrDefault(u => u.Name == name);
            if (result != null)
            {
                return result.Value;
            }
            var value = Utils.GetConfig("AppSettings:" + name);
            return value;
        }
        public string GetConfig(string name)
        {

            var result = Collection.FirstOrDefault(u => u.Name == name);
            if (result != null)
            {
                return result.Value;
            }
            var value = Utils.GetConfig("AppSettings:" + name);
            return value;
        }

        public BaseParameterModel GetModel(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result;
        }
    }
}