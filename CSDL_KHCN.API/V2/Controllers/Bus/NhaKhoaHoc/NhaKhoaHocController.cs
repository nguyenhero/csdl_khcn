﻿
using CSDL_KHCN.Business_V2;
using CSDL_KHCN.Data_V2;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CSDL_KHCN.API_V2
{
    /// <inheritdoc />
    /// <summary>
    /// Module tham số hệ thống
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/bus/nha_khoa_hoc")]
    [ApiExplorerSettings(GroupName = "03: BUS NhaKhoaHoc")]
    public class BusNhaKhoaHocController : ControllerBase
    {
        private readonly INhaKhoaHocHandler _parameterHandler;

        public BusNhaKhoaHocController(INhaKhoaHocHandler parameterHandler)
        {
            _parameterHandler = parameterHandler;
        }
        #region CRUD
        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<NhaKhoaHocModel>), StatusCodes.Status200OK)]
//        [SwaggerRequestExample(typeof(NhaKhoaHocCreateModel), typeof(MockupObject<NhaKhoaHocCreateModel>))]
        public async Task<IActionResult> CreateAsync([FromBody] NhaKhoaHocCreateModel model)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _parameterHandler.CreateAsync(model,appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Cập nhật
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
//        [SwaggerRequestExample(typeof(NhaKhoaHocUpdateModel), typeof(MockupObject<NhaKhoaHocUpdateModel>))]
        public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] NhaKhoaHocUpdateModel model)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _parameterHandler.UpdateAsync(id, model,appId, actorId);

            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}")]
        [ProducesResponseType(typeof(ResponseDelete), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            // Get Token Info
//            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
//            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _parameterHandler.DeleteAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa danh sách
        /// </summary>
        /// <param name="listId">Danh sách id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseDeleteMulti), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRangeAsync([FromQuery]List<Guid> listId)
        {
            // Get Token Info
//            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
//            var appId = requestInfo.ApplicationId;
            // Call service 
            var result = await _parameterHandler.DeleteRangeAsync(listId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo Id
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<NhaKhoaHocModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(Guid id)
        {
            // Get Token Info
//            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
//            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _parameterHandler.FindAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo bộ loc
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        /// <remarks>
        ///  *filter*
        ///  ....
        ///  *sort*
        ///  ....
        /// </remarks>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponsePagination<NhaKhoaHocModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync([FromQuery]int page = 1, [FromQuery]int size = 20, [FromQuery]string filter = "{}", [FromQuery]string sort = "")
        {
            // Get Token Info
//            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
//            var appId = requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<NhaKhoaHocQueryModel>(filter);
            filterObject.Sort = sort;
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _parameterHandler.GetPageAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về tất cả
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseObject<IList<NhaKhoaHocModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            // Get Token Info
//            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
//            var actorId = requestInfo.UserId;
//            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _parameterHandler.GetAllAsync();
            // Hander response
            return Helper.TransformData(result);
        }
        #endregion 
    }
}
