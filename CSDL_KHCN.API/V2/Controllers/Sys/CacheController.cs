﻿using System.Collections.Generic;
using CSDL_KHCN.API_V2;
using CSDL_KHCN.Business_V2;
using CSDL_KHCN.Data_V2;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CSDL_KHCN.API.V2
{

    /// <summary>
    ///  Cache hệ thống
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/cache")]
    [ApiExplorerSettings(GroupName = "01: System - Cache")]

    public class AdminCacheController : ControllerBase
    {

        /// <summary>
        /// Kiểm tra cache hệ thống
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        [Authorize, HttpGet, Route("")]
        public IActionResult GetCollection()
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service 
            var dictionary =
                new Dictionary<string, object>
                {
                    ["ApplicationCollection"] = ApplicationCollection.Instance.Collection,
                    ["ParameterCollection"] = ParameterCollection.Instance.Collection,
                    ["UserCollection"] = UserCollection.Instance.Collection,
                    ["RoleCollection"] = RoleCollection.Instance.Collection,
                };
            var result = new ResponseObject<Dictionary<string, object>>(  dictionary);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Làm mới cache hệ thống
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        [Authorize, HttpGet, Route("reload")]
        public IActionResult ReloadCollection()
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service 
            ApplicationCollection.Instance.LoadToHashSet();
            ParameterCollection.Instance.LoadToHashSet();
            UserCollection.Instance.LoadToHashSet();
            RoleCollection.Instance.LoadToHashSet();
            var dictionary =
                new Dictionary<string, object>
                {
                    ["ApplicationCollection"] = ApplicationCollection.Instance.Collection,
                    ["ParameterCollection"] = ParameterCollection.Instance.Collection,
                    ["UserCollection"] = UserCollection.Instance.Collection,
                    ["RoleCollection"] = RoleCollection.Instance.Collection,
                };
            var result = new ResponseObject<Dictionary<string, object>>( dictionary);
            // Hander response
            return Helper.TransformData(result);
        }
    }
}