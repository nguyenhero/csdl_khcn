﻿using CSDL_KHCN.API;
using CSDL_KHCN.API.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSDL_KHCN.API_V2
{

    /// <inheritdoc />
    /// <summary>
    /// Trạng thái API
    /// </summary> 
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/status")]
    [ApiExplorerSettings(GroupName = "01: System - Status")]
    public class AdminStatusController : ControllerBase
    {
        private readonly IEnumerable<IHealthChecker> _healthCheckers;

        public AdminStatusController(IEnumerable<IHealthChecker> healthCheckers) =>
            _healthCheckers = healthCheckers;

        /// <summary>
        /// Gets the status of this API and it's dependencies, giving an indication of it's health.
        /// </summary>
        /// <returns>A 204 OK or error response containing details of what is wrong.</returns>
        /// <response code="204">The API is functioning normally.</response>
        /// <response code="503">The API or one of it's dependencies is not functioning, the service is unavailable.</response>
        [HttpGet(Name = StatusControllerRoute.GetStatus)]
        [Authorize]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(void), StatusCodes.Status503ServiceUnavailable)]
        public async Task<IActionResult> GetStatus()
        {
            try
            {
                var tasks = _healthCheckers.Select(healthChecker => healthChecker.CheckHealth()).ToList();

                await Task.WhenAll(tasks);
            }
            catch
            {
                return new StatusCodeResult(StatusCodes.Status503ServiceUnavailable);
            }

            return new NoContentResult();
        }
    }
}