﻿using CSDL_KHCN.Business_V2;
using CSDL_KHCN.Data_V2;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CSDL_KHCN.API_V2
{
    /// <summary>
    /// JWT cho hệ thống
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/jwt/token")]
    [ApiExplorerSettings(GroupName = "01: System - JWT")]
    public class AdminTokenController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IUserHandler _userHandler;
        private readonly IApplicationHandler _applicationHandler;

        public AdminTokenController(IConfiguration config, IUserHandler userHandler, IApplicationHandler applicationHandler)
        {
            _config = config;
            _userHandler = userHandler;
            _applicationHandler = applicationHandler;
        }

        /// <summary>
        /// Đăng nhập và lấy kết quả JWT token
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [AllowAnonymous, HttpPost]
        [SwaggerRequestExample(typeof(LoginModel), typeof(LoginModelExample))]
        public async Task<IActionResult> SignInJwt([FromBody] LoginModel login)
        {
            IActionResult response = Unauthorized();
            if  (login.Username == _config["Authencate:AdminUser"] &&
                 login.Password == _config["Authencate:AdminPassWord"]) 
            {
                var tokenString = BuildToken(new UserModel()
                {
                    AvatarUrl = "",
                    UserName = _config["Authencate:AdminUser"],
                    Email = "admin@admin.com",
                    LastActivityDate = DateTime.Now,
                    PhoneNumber = "967267469",
                    Type = 1,
                    Id = UserConstants.AdministratorId

                });
                 response = Helper.TransformData(new ResponseObject<LoginResponse>(new LoginResponse()
                        {
                            TokenString = tokenString,
                            UserId = UserConstants.AdministratorId,
                            ApplicationModel = ApplicationCollection.Instance.GetModel(AppConstants.RootAppId),
                            ApplicationId = AppConstants.RootAppId,
                            TimeExpride = DateTime.Now.AddMinutes(Convert.ToDouble(_config["Authencate:Jwt:TimeToLive"]))
                        }));
                return response;
            }
            if  (login.Username == _config["Authencate:GuestUser"] && login.Password == _config["Authencate:GuestPassWord"]) 
            {
                var tokenString = BuildToken(new UserModel()
                {
                    AvatarUrl = "",
                    UserName = _config["Authencate:GuestUser"],
                    Email = "guest@admin.com",
                    LastActivityDate = DateTime.Now,
                    PhoneNumber = "967267469",
                    Type = 1,
                    Id = UserConstants.UserId

                });
                response = Helper.TransformData(new ResponseObject<LoginResponse>(new LoginResponse()
                {
                    TokenString = tokenString,
                    UserId = UserConstants.UserId,
                    ApplicationModel = ApplicationCollection.Instance.GetModel(AppConstants.RootAppId),
                    ApplicationId = AppConstants.RootAppId,
                    TimeExpride = DateTime.Now.AddMinutes(Convert.ToDouble(_config["Authencate:Jwt:TimeToLive"]))
                }));
                return response;
            }




            var user =  _userHandler.Authencate(login.Username, login.Password);
            if (user.Code == Code.Success && user is ResponseObject<UserModel> userData)
            {
                //Danh sách ứng dụng
                var getApp = await _applicationHandler.GetDefautByUserIdAsync(userData.Data.Id);
                if (getApp.Code == Code.Success && getApp is ResponseObject<ApplicationModel> getAppData)
                {
                        var tokenString = BuildToken(userData.Data);
                        response = Helper.TransformData(new ResponseObject<LoginResponse>(new LoginResponse()
                        {
                            TokenString = tokenString,
                            UserId = userData.Data.Id,
                            ApplicationModel = getAppData.Data,
                            ApplicationId = getAppData.Data.Id,
                            TimeExpride = DateTime.Now.AddMinutes(Convert.ToDouble(_config["Authencate:Jwt:TimeToLive"]))
                        }));
                    return response;
                }
                else
                {
                    response = Helper.TransformData(new ResponseError(Code.Forbidden,
                        "Người dùng chưa được cấp quyền vào ứng dụng nào"));
                }
            }
            return response;
        }

        private string BuildToken(BaseUserModel user)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.NameId, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Authencate:Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Authencate:Jwt:Issuer"],
                _config["Authencate:Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(Convert.ToDouble(_config["Authencate:Jwt:TimeToLive"])),
                signingCredentials: creds);
            return new JwtSecurityTokenHandler().WriteToken(token);

        }

        public class LoginModel
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        public class LoginModelExample : IExamplesProvider
        {
            public object GetExamples()
            {
                return new LoginModel
                {
                    Username = "Admin",
                    Password = "1Qaz2wsx"
                };
            }
        }

        public class LoginResponse
        {
            public Guid UserId { get; set; }
            public BaseUserModel UserModel => UserCollection.Instance.GetModel(UserId);
            public string TokenString { get; set; }
            public DateTime TimeExpride { get; set; }
            public BaseApplicationModel ApplicationModel { get; set; }
            public Guid ApplicationId { get; set; }
        }
    }
}