﻿using CSDL_KHCN.Business_V2;
using CSDL_KHCN.Data_V2;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CSDL_KHCN.API_V2
{
    /// <inheritdoc />
    /// <summary>
    /// Module người dùng
    /// </summary>
    [ApiVersion("2.0")]
    [Route("api/v{api-version:apiVersion}/idm/users")]
    [ApiExplorerSettings(GroupName = "04: IDM User")]
    public class IdmUserController : ControllerBase
    {
        private readonly IUserHandler _userHandler;
        private readonly IUserMapRoleHandler _userMapRoleHandler;
        public IdmUserController(IUserHandler userHandler, IUserMapRoleHandler userMapRoleHandler)
        {
            _userHandler = userHandler;
            _userMapRoleHandler = userMapRoleHandler;
        }

        #region CRUD

        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <param name="applicationId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<UserModel>), StatusCodes.Status200OK)]
        // [SwaggerRequestExample(typeof(UserCreateModel), typeof(MockupObject<UserCreateModel>))]
        public async Task<IActionResult> CreateAsync([FromBody] UserCreateModel model, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.CreateAsync(model, appId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Cập nhật mật khẩu
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="password">Mật khẩu mới</param>
        /// <param name="applicationId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}/changepassword")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<IActionResult> ChangePasswordAsync(Guid id, [FromQuery] string password, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.ChangePasswordAsync(id, password, appId);

            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Cập nhật
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="model">Dữ liệu</param>
        /// <param name="applicationId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        // [SwaggerRequestExample(typeof(UserUpdateModel), typeof(MockupObject<UserUpdateModel>))]
        public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] UserUpdateModel model, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.UpdateAsync(id, model, appId);

            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}")]
        [ProducesResponseType(typeof(ResponseDelete), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            // Get Token Info
            //            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //            var actorId = requestInfo.UserId;
            //            var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.DeleteAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa danh sách
        /// </summary>
        /// <param name="listId">Danh sách id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseDeleteMulti), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRangeAsync([FromQuery]List<Guid> listId)
        {
            // Get Token Info
            //            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //            var actorId = requestInfo.UserId;
            //            var appId = applicationId??requestInfo.ApplicationId;
            // Call service 
            var result = await _userHandler.DeleteRangeAsync(listId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo Id
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<UserModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(Guid id)
        {
            // Get Token Info
            //            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //            var actorId = requestInfo.UserId;
            //            var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.FindAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo bộ loc
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        /// <remarks>
        ///  *filter*
        ///  ....
        ///  *sort*
        ///  ....
        /// </remarks>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponsePagination<UserModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync([FromQuery]int page = 1, [FromQuery]int size = 20, [FromQuery]string filter = "{}", [FromQuery]string sort = "")
        {
            // Get Token Info
            //            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //            var actorId = requestInfo.UserId;
            //            var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<UserQueryModel>(filter);
            filterObject.Sort = sort;
            if (string.IsNullOrEmpty(filterObject.Sort))
            {
                filterObject.Sort = "+LastModifiedOnDate";
            }
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _userHandler.GetPageAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về tất cả
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseObject<IList<UserModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync()
        {
            // Get Token Info
            //            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //            var actorId = requestInfo.UserId;
            //            var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.GetAllAsync();
            // Hander response
            return Helper.TransformData(result);
        }
        #endregion
        /// <summary>
        /// Kiểm tra tên
        /// </summary>
        /// <param name="name"></param>
        /// <param name="applicationId"></param> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("checkname/{name}")]
        [ProducesResponseType(typeof(ResponseObject<IList<UserDetailModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckNameAvailability(string name, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.CheckNameAvailability(name, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về chi tiết
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}/detail")]
        [ProducesResponseType(typeof(ResponseObject<IList<UserDetailModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetDetail(Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.GetDetail(id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về danh sách nhóm của người dùng
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}/role")]
        [ProducesResponseType(typeof(ResponseObject<IList<BaseRoleModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetRoleMapUserAsync(Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userMapRoleHandler.GetRoleMapUserAsync(id, appId);
            // Hander response
            return Helper.TransformData(result);
        }




        /// <summary>
        /// Gán người dùng vào nhóm
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listRoleId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("{id}/role")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> AddUserMapRoleAsync([FromBody]IList<Guid> listRoleId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userMapRoleHandler.AddUserMapRoleAsync(listRoleId, id, appId, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }


        /// <summary>
        /// Gỡ người khỏi vào nhóm
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listRoleId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}/role")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteUserMapRoleAsync([FromBody]IList<Guid> listRoleId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            //            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userMapRoleHandler.DeleteUserMapRoleAsync(listRoleId, id, appId);
            // Hander response
            return Helper.TransformData(result);
        }
    }
}
