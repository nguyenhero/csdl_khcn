using CSDL_KHCN.Business_V2;
using CSDL_KHCN.Data_V2;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Serilog;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CSDL_KHCN.API_V2
{
    public static class Helper {
        /// <summary>
        /// Transform data to http response
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static IActionResult TransformData(Response data) {
            var result = new ObjectResult(data) {StatusCode = (int) data.Code};
            return result;
        }

        /// <summary>
        /// Get user info in token and headder
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public static RequestUser GetRequestInfo(HttpRequest request, ClaimsPrincipal currentUser)
        {

            var result = new RequestUser
            {
                UserId = Guid.Empty,
                ApplicationId = Guid.Empty
            };
            request.Headers.TryGetValue("X-ApplicationId", out StringValues applicationId);
            if (!string.IsNullOrEmpty(applicationId) && Utils.IsGuid(applicationId))
            {
                result.ApplicationId = new Guid(applicationId);
            }
            if (currentUser.HasClaim(c => c.Type == ClaimTypes.NameIdentifier))
            {
                var userId = currentUser.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
                if (!string.IsNullOrEmpty(userId) && Utils.IsGuid(userId))
                {
                    result.UserId = new Guid(userId);
                }
            }
            return result;
        }
    }
    public class RequestUser {
        public Guid UserId {get; set;}
        public Guid ApplicationId {get; set;}
    }
     public static class CSDL_KHCNHelper
    {
        public static string MakePrefix(Guid? appId)
        {
            if (appId.HasValue)
            {
                var app = ApplicationCollection.Instance.GetModel(appId.Value);
                if (app != null)
                    return "_" + app.Code;
            }

            return null;
        }
        public static string GetTrueName(string name)
        {
            switch (name)
            {
                case "Extension": return "Định dạng";
                case "Path": return "Đường dẫn";
                case "Status": return "Trạng thái";
                case "Level": return "Định dạng";
                case "Content": return "Nội dung";
                case "Tags": return "Tag";
            }
            if (name.Contains("Metadata.")) name = name.Replace("Metadata.", "");

            return name;
        }
        private static Image ResizeImage(Image imgToResize, Size size, bool isCenterCrop = false)
        {
            //Width thực
            var sourceWidth = imgToResize.Width;
            //Height thực
            var sourceHeight = imgToResize.Height;
            if (isCenterCrop)
            {
                var nPercentW = size.Width / (float)sourceWidth;
                var nPercentH = size.Height / (float)sourceHeight;

                var nPercent = nPercentH < nPercentW ? nPercentH : nPercentW;
                //Chiều rộng mới
                var destWidth = (int)(sourceWidth * nPercent);
                //Chiều cao mới
                var destHeight = (int)(sourceHeight * nPercent);
                var b = new Bitmap(destWidth, destHeight);
                var g = Graphics.FromImage(b);
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
                g.Dispose();
                return b;
            }
            else
            {
                var b = new Bitmap(size.Width, size.Height);
                var g = Graphics.FromImage(b);
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(imgToResize, 0, 0, size.Width,size.Height);
                g.Dispose();
                return b;
            }



        }

        public static async Task<Image> ThumbnailImageAsync(string originalImagePath,
            string saveFilePath, int width,
            int height)
        {
            try
            {
                var wc = new WebClient();
                var bytes = await wc.DownloadDataTaskAsync(originalImagePath);
                var ms = new MemoryStream(bytes);
                var originalImage = Image.FromStream(ms);
                var imThumbnailImage = ResizeImage(originalImage, new Size(width, height));
                imThumbnailImage.Save(saveFilePath, ImageFormat.Jpeg);
                imThumbnailImage.Dispose();
                originalImage.Dispose();
                return imThumbnailImage;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return null;
            }
        }

    }

}
