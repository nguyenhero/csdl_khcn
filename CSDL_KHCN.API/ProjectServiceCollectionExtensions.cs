using CSDL_KHCN.Business_V2;

namespace CSDL_KHCN.API
{
    using CSDL_KHCN.API.Services;
    using Microsoft.Extensions.DependencyInjection;

    /// <summary>
    /// <see cref="IServiceCollection"/> extension methods add project services.
    /// </summary>
    /// <remarks>
    /// AddSingleton - Only one instance is ever created and returned.
    /// AddScoped - A new instance is created and returned for each request/response cycle.
    /// AddTransient - A new instance is created and returned each time.
    /// </remarks>
    public static class ProjectServiceCollectionExtensions
    {
        public static IServiceCollection AddProjectServices(this IServiceCollection services) =>
            services
        #region BSD

                .AddSingleton<IParameterHandler, ParameterHandler>()

        #endregion

        #region SYS

                .AddSingleton<IApplicationHandler, ApplicationHandler>()

        #endregion

        #region IDM

                .AddSingleton<IRoleHandler, RoleHandler>()
                .AddSingleton<IUserHandler, UserHandler>()
                .AddSingleton<IUserMapRoleHandler, UserMapRoleHandler>()


        #endregion
        #region BUS

                .AddSingleton<IHoiDongKhoaHocHandler, HoiDongKhoaHocHandler>()
                .AddSingleton<INhaKhoaHocHandler, NhaKhoaHocHandler>()

        #endregion
                .AddSingleton<IClockService, ClockService>();
    }
}