﻿var path = require("path");
module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        //Hint all file
        jshint: {
            all: ['Gruntfile.js', 'app-data/**/*.js'],
            dev: {
                files: [{
                    expand: true,
                    cwd: '.',
                    src: 'app-data/**/*.js',
                    dest: 'dist'
                }]
            }
        },
        //Step1: Xóa thư mục deploy
        clean: {
            build: ['dist'],
            build2nd: ['dist/app-data', 'dist/index.html', 'dist/favicon.ico']
        },
        //Step2: Copy all dữ liệu sang folder dist
        copy: {
            build: {
                expand: true,
                cwd: '.',
                src: ['app-data/**/*', 'assets/**/*', 'node_modules/**/*', 'index.html', 'favicon.ico'],
                dest: 'dist/'
            },
            build2nd: {
                expand: true,
                cwd: '.',
                src: ['app-data/**/*', 'index.html', 'favicon.ico'],
                dest: 'dist/'
            }
        },
        //Step 3 Annotate file
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            build: {
                files: [{
                    expand: true,
                    src: ['dist/app-data/**/*.js']
                }]
            },
            dev: {
                files: [{
                    expand: true,
                    src: ['app-data/**/*.js']
                }]
            }
        },
        //Step 3 Minifi js file
        uglify: {
            build: {
                options: {
                    mangle: {
                        reserved: ['jQuery', 'Backbone', 'angular']
                    },
                    options: {
                        banner: '/*! loki did it */'
                    },
                    sourceMap: true,
                    sourceMapName: 'sourcemap/uglify.map'
                },
                files: [{
                    expand: true,
                    src: ['dist/app-data/**/*.js'],

                }]
            },
            dev: {
                options: {
                    mangle: {
                        reserved: ['jQuery', 'Backbone', 'angular']
                    },
                    options: {
                        banner: '/*! loki did it */'
                    },
                    sourceMap: true,
                    sourceMapName: 'sourcemap/uglify.map'
                },
                files: [{
                    expand: true,
                    cwd: '.',
                    src: 'app-data/**/*.js',
                    dest: 'dist'
                }]
            }
        },
        //Step 4 Minifi html file
        htmlmin: {
            build: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    expand: true,
                    src: ['dist/app-data/**/*.html', 'dist/index.html']
                }]
            },
            dev: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    expand: true,
                    cwd: '.',
                    src: 'app-data\\views\\business\\control\\detai\\detaidetails.html',
                    // src: 'app-data/**/*.html',
                    dest: 'dist'
                }]
            }

        },

        //Step 5 Compile scss
        compass: {
            build: {
                options: {
                    sassDir: 'assets/scss',
                    cssDir: 'assets/css'
                }
            },
            dev: {
                options: {
                    sassDir: 'assets/scss',
                    cssDir: 'assets/css'
                }
            }
        },
        //Step 6 mini file css
        postcss: {
            build: {
                options: {
                    map: {
                        inline: false, // save all sourcemaps as separate files...
                        annotation: 'dist/assets/maps/' // ...to the specified directory
                    },
                    processors: [
                        require('pixrem')(), // add fallbacks for rem units
                        require('autoprefixer')({
                            browsers: 'last 2 versions'
                        }), // add vendor prefixes
                        require('cssnano')() // minify the result
                    ]
                },
                dist: {
                    src: ['dist/app-data/**/*.css'],
                }
            },
            dev: {
                options: {
                    map: {
                        inline: false, // save all sourcemaps as separate files...
                        annotation: 'dist/assets/css/maps/' // ...to the specified directory
                    },
                    processors: [
                        require('pixrem')(), // add fallbacks for rem units
                        require('autoprefixer')({
                            browsers: 'last 2 versions'
                        }), // add vendor prefixes
                        require('cssnano')() // minify the result
                    ]
                },
                dist: {
                    cwd: '.',
                    src: 'app-data/**/*.js',
                    dest: 'dist'
                }
            }
        },
        //Step 4 server
        connect: {
            dev: {
                options: {
                    hostname: 'localhost',
                    port: 3005,
                    debug: true,
                    base: '.'
                }
            }
        },

        //Extra
        imagemin: {
            img: {
                options: {
                    optimizationLevel: 5,
                    quality: 75
                },
                files: [{
                    expand: true,
                    cwd: 'assets/img/',
                    src: '**',
                    dest: 'dist/assets/img/'
                }]
            }
        },
        watch: {
            options: {
                livereload: true,
                spawn: false,
            },
            js: {
                files: ["app-data/**/*.js"],
                tasks: ["uglify:dev", 'jshint:dev']
            },
            html: {
                files: ["app-data/**/*.html", "index.html"],
                tasks: ["htmlmin:dev"]
            },
            css: {
                files: ["assets/**/*.scss", "assets/**/*.css"],
                tasks: ["compass:dev", "postcss:dev"]
            }

        },
    });
    require('load-grunt-tasks')(grunt);
    // Actually running things. 
    grunt.loadNpmTasks('grunt-notify');
    grunt.event.on('watch', function (action, filepath, target) {
        switch (target) {
            case 'js':
                var configJshint = grunt.config("jshint");
                configJshint.dev.files[0].src = path.relative(".", filepath);
                grunt.config("jshint", configJshint);
                // -----------------------
                var configUglify = grunt.config("uglify");
                configUglify.dev.files[0].src = path.relative(".", filepath);
                grunt.config("uglify", configUglify);
                break;
            case 'html':
                var configHtmlmin = grunt.config("htmlmin");
                configHtmlmin.dev.files[0].src = path.relative(".", filepath);
                grunt.config("htmlmin", configHtmlmin);
                break;
            case 'css':
                var configCompass = grunt.config("compass");
                configCompass.dev.files[0].src = path.relative(".", filepath);
                grunt.config("compass", configCompass);
                // ------------
                var configPostcss = grunt.config("postcss");
                configPostcss.dev.dist.src = path.relative(".", filepath);
                grunt.config("postcss", configPostcss);
                break;

            default:
                break;
        }

    });

    // Default task(s).
    grunt.registerTask('build', ['clean:build', 'copy:build', 'ngAnnotate:build', 'uglify:build', 'htmlmin:build', 'postcss:build']);
    grunt.registerTask('build2nd', ['clean:build2nd', 'copy:build2nd', 'ngAnnotate:build', 'uglify:build', 'htmlmin:build', 'postcss:build']);
    grunt.registerTask('default', ['watch']);

};