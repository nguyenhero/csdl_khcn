/* 
 * Main function Require
 * By Savis Software Team
 * Author: DuongPD | NguyenBv
 */
require.config({
    baseUrl: "/app-data/mandatory-js",
    urlArgs: "bust=v1.2.14",
    paths: {
        /* 1.CORE PLUGINS */
        "jquery": ["../../node_modules/jquery/dist/jquery.min",
            "../../node_modules/jQuery/dist/jquery.min",
            "../../node_modules/jquery/jquery.min"
        ],
        "jquery-slimscroll": "../../node_modules/jquery-slimscroll/jquery.slimscroll.min",
        "jquery-cookie": "../../node_modules/js-cookie/src/js.cookie.min",
        "jquery-ui": "../../node_modules/jquery-ui-dist/jquery-ui.min",

        "bootstrap": "../../node_modules/bootstrap/dist/js/bootstrap.min",
        "bootstrap-switch": "../../node_modules/bootstrap-switch/dist/js/bootstrap-switch.min",
        "bootstrap-switch": "../../node_modules/bootstrap-switch/dist/js/bootstrap-switch.min",
        // "bootstrap-session-timeout": "../../node_modules/bootstrap-session-timeout/dist/bootstrap-session-timeout.min",
        "bootstrap-hover-dropdown": "../../node_modules/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min",

        "angular": "../../node_modules/angular/angular.min",
        "angularAMD": "../../node_modules/angular-amd/angularAMD.min",
        "angular-message": "../../node_modules/angular-messages/angular-messages.min",
        "angular-sanitize": "../../node_modules/angular-sanitize/angular-sanitize.min",
        "angular-route": "../../node_modules/angular-route/angular-route.min",
        "angular-ui-router": "../../node_modules/angular-ui-router/release/angular-ui-router.min",
        "state-event": "../../node_modules/angular-ui-router/release/stateEvents.min",
        "angular-loading-bar": "../../node_modules/angular-loading-bar/src/loading-bar",
        "ui-select": "../../node_modules/ui-select/dist/select.min",
        "angular-moment": "../../node_modules/angular-moment/angular-moment",
        "angular-toastr": "../../node_modules/angular-toastr/dist/angular-toastr.tpls.min",
        "angular-base64": "../../node_modules/angular-base64/angular-base64.min",

        "angular-datepicker": "../../node_modules/angularjs-datepicker/dist/angular-datepicker.min",
        "bootstrap-daterangepicker": "../../node_modules/bootstrap-daterangepicker/daterangepicker",

        "ui-bootstrap": "../../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls",
        'jwplayer': '../../assets/global/plugins/jwplayer/jwplayer',
        // 'jwplayer': '../../node_modules/JW Player Mirror/dist/jwplayer',
        "moment": "../../node_modules/moment/min/moment.min",
        "blockui": "../../node_modules/blockui/jquery.blockUI",
        "oclazyload": "../../node_modules/oclazyload/dist/ocLazyLoad.min",
        "ngfileupload": "../../node_modules/ng-file-upload/dist/ng-file-upload.min",

        /* 3.CUSTOM PLUGINS */
        "directives-common": "../components/directives/directives-common",
        "env": "../components/env/env",
        "config-route": "../components/config/config-route",
        "config-auth": "../components/config/config-auth",
        "service-api": "../components/services/service-api",
        "service-amd": "../components/services/service-amd",
        "constant-app": "../components/constants/constant-app",
        //* 3.TEMPLETE PLUGINS */ 
        "HeaderModule": "../views/template/header/header",
        "FooterModule": "../views/template/footer/footer",
        "SidebarModule": "../views/template/sidebar/sidebar",
        // Core script to handle the entire theme and core functions
        "App": "../../assets/global/scripts/app",
        "Layout": "../../assets/layouts/layout/scripts/layout",
    },
    shim: {
        "jquery": {
            exports: '$'
        },
        "moment": {
            exports: 'moment'
        },
        "angular": {
            deps: ["jquery"],
            exports: 'angular'
        },
        "jquery-ui": {
            deps: ["jquery"]
        },
        "angularAMD": {
            deps: ["angular"]
        },
        "angular-ui-router": {
            deps: ["angular"]
        },
        "angular-route": {
            deps: ["angular"]
        },
        "state-event": {
            deps: ["angular", "angular-ui-router"]
        },

        "ui-select": {
            deps: ["angular"]
        },
        "angular-loading-bar": {
            deps: ["angular"]
        },
        "angular-base64": {
            deps: ["angular"]
        },
        "ui-bootstrap": {
            deps: ["angular"]
        },
        "oclazyload": {
            deps: ["jquery", "angular"]
        },
        "angular-sanitize": {
            deps: ["angular"]
        },
        "jquery-slimscroll": {
            deps: ["angular", "jquery"]
        },
        "angular-datepicker": {
            deps: ["angular"]
        },
        "bootstrap": {
            deps: ["jquery"]
        },
        // "bootstrap-session-timeout": {
        //     deps: ["jquery", "bootstrap"]
        // },
        "bootstrap-hover-dropdown": {
            deps: ["jquery"]
        },
        "App": {
            deps: ["jquery", "bootstrap", "jquery-slimscroll", "config-auth"]
        },
        "Layout": {
            deps: ["App"]
        },
        "angular-toastr": {
            deps: ["angular"]
        },
        "ngfileupload": ['angular'],
        "angular-moment": {
            deps: ["moment", "angular"]
        },
        "angular-message": {
            deps: ["angular"]
        },
        'jwplayer': {
            exports: 'jwplayer'
        },
    },
    deps: ["app"]
});