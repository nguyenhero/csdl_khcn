/// <reference path="main.js" />
define([
    "angularAMD",
    "jquery",
    "env",
    "config-route",
    "config-auth",
    "SidebarModule",
    "HeaderModule",
    "directives-common",
    "angular-ui-router",
    "angular-route",
    "angular-message",
    "jquery-ui",
    "angular-loading-bar",
    "state-event",
    "service-api",
    "service-amd",
    "ngfileupload",
    "constant-app",
    "oclazyload",
    "angular-toastr",
    "angular-sanitize",
    "ui-select",
    "angular-base64",
    "App",
    "Layout",
    "FooterModule",
    "ui-bootstrap",
    "jwplayer",
], function (angularAMD, $, env, routeConfig, authConfig, sidebarModule) {
    "use strict";
    jwplayer.key = "CKjOe06GxAOe3Dj9NaWPCQKtqvqQdyFV8z9wsg==";
    var CSDL_KHCNApp = angular.module("CSDL_KHCNApp", [
        "ui.router",
        "ngRoute",
        "ngMessages",
        "ui.router.state.events",
        "ui.select",
        "oc.lazyLoad",
        "ngSanitize",
        "toastr",
        "base64",
        "angular-loading-bar",
        "ngFileUpload",
        "ui.bootstrap",
    ]);
    CSDL_KHCNApp.config(["$qProvider", function ($qProvider) {
        if (env.ENV === "production") {
            $qProvider.errorOnUnhandledRejections(false);
        }
    }]);
    /* Setup global settings */
    CSDL_KHCNApp.factory("settings", ["$rootScope", function ($rootScope) {
        // supported languages
        var settings = {
            layout: {
                pageSidebarClosed: false, // sidebar menu state
                pageContentWhite: true, // set page content layout
                pageBodySolid: false, // solid body color state
                pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
            },
            assetsPath: "../../assets",
            globalPath: "../../assets/global",
            layoutPath: "../../assets/layouts/layout",
        };

        $rootScope.settings = settings;

        return settings;
    }]);
    // #region Setup Auth
    CSDL_KHCNApp.IsAuthenMode = (env.AUTH_MODE !== "NONE");
    // #region Init user info
    var initUserInfo = function () {
        CSDL_KHCNApp.CurrentUser = {
            Id: window.localStorage["user_id"],
            //UserName: window.localStorage["user_name"],
            DisplayName: window.localStorage["display_name"],
            ApplicationId: window.localStorage["app_id"]
        };
        if (!CSDL_KHCNApp.IsAuthenMode) {
            CSDL_KHCNApp.CurrentUser.Id = "00000000-0000-0000-0000-000000000001";
            CSDL_KHCNApp.CurrentUser.DisplayName = "Administrator";
            CSDL_KHCNApp.CurrentUser.ApplicationId = "00000000-0000-0000-0000-000000000001";
            if (!window.localStorage["access_token"])
                window.localStorage["access_token"] = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiIwMDAwMDAwMC0wMDAwLTAwMDAtMDAwMC0wMDAwMDAwMDAwMDEiLCJqdGkiOiJlOWE2MzUyNy0wNDA4LTRlMDYtYThiNS01MDhlMmE0NTZiZWYiLCJleHAiOjE1MzAyNTA2NTIsImlzcyI6IlNBVklTIENPUlAiLCJhdWQiOiJTQVZJUyBDT1JQIn0.ZCg_02_256_TsJ9YGhYnJERUQpAlkE7U7xCqdKw7Oe0"
            authConfig.setupNonAuth(CSDL_KHCNApp);
        }
    };
    /* Setup app version */
    CSDL_KHCNApp.Version = "?bust=v1.2.14";
    var currenthash = window.location.hash;
    if (currenthash.includes("#!/public")) {
        CSDL_KHCNApp.IsAuthenMode = false;
        CSDL_KHCNApp.Mode = "public";
        // 1. Init config add to constant of app module
        authConfig.initNonAuthConfig(CSDL_KHCNApp);
        $("#headerDiv").css("display", "none");
        $("#footerDiv").css("display", "none");
        $("#sidebarDiv").css("display", "none");
        $("#loadingDiv").css("display", "none");
        $("#view").css("position", "fixed");
        $("#view").css("top", "0");
        $("#view").css("left", "0");
        $("#view").css("right", "0");
        $("#view").css("bottom", "0");
        // Setup route
        authConfig.setupNonAuth(CSDL_KHCNApp);
        routeConfig.setupRouteConfig(CSDL_KHCNApp);
    } else {
        CSDL_KHCNApp.Mode ="authed";
        if (CSDL_KHCNApp.IsAuthenMode) {
            // 1. Init config add to constant of app module
            authConfig.initAuthConfig(CSDL_KHCNApp);
            // Check share feauture 
            // 2. Check auth in local storage
            var hasAuthToken = authConfig.checkAuthToken(CSDL_KHCNApp);
            if (hasAuthToken) {
                // Setup auth
                authConfig.setupAuth(CSDL_KHCNApp, $);
                // JWT disable
                var siteId = window.localStorage["app_id"];
                var validUser = window.localStorage["valid_user"];
                // Setup config for Routing 
                if (siteId === null || typeof siteId === "undefined" || siteId === "undefined" || validUser === "0") {
                    routeConfig.setupSiteAccessConfig(CSDL_KHCNApp);
                } else {
                    //Show template
                    $("#headerDiv").css("display", "");
                    $("#footerDiv").css("display", "");
                    $("#sidebarDiv").css("display", "");
                    setTimeout(function () {
                        $("#loadingDiv").css("display", "none");
                    }, 0);
                    routeConfig.setupRouteConfig(CSDL_KHCNApp);
                }
            } else {
                $("#headerDiv").css("display", "none");
                $("#footerDiv").css("display", "none");
                $("#sidebarDiv").css("display", "none");
                $("#loadingDiv").css("display", "none");
                routeConfig.setupSiteAccessConfig(CSDL_KHCNApp);
                authConfig.toLoginForm();
            }
        } else {
            authConfig.initNonAuthConfig(CSDL_KHCNApp);
            $("#headerDiv").css("display", "none");
            $("#footerDiv").css("display", "none");
            $("#sidebarDiv").css("display", "none");
            $("#loadingDiv").css("display", "none");
            // Setup route
            authConfig.setupNonAuth(CSDL_KHCNApp);
            routeConfig.setupRouteConfig(CSDL_KHCNApp);
        }
    }
    initUserInfo();


    // #endregion

    // #region Setup loading bar
    CSDL_KHCNApp.config(["cfpLoadingBarProvider", function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = true;
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.spinnerTemplate = "<div id=\"loading-bar-spinner\">Đang nạp dữ liệu, vui lòng đợi ...</div>";
    }]);
    // #endregion

    // #region Setup Notifications
    CSDL_KHCNApp.config(['toastrConfig', function (toastrConfig) {
        angular.extend(toastrConfig, {
            autoDismiss: false,
            tapToDismiss: true,
            closeButton: true,
            progressBar: true,
            timeOut: 5000,
            closeHtml: "<button>&times;</button>",
            containerId: "toast-container",
            maxOpened: 0,
            newestOnTop: true,
            positionClass: "toast-bottom-right",
            preventDuplicates: false,
            preventOpenDuplicates: false,
            target: "body"
        });
    }]);
    // #endregion


    CSDL_KHCNApp.run(["$rootScope", "settings", "$state", "$http", "$q", "UtilsService", function ($rootScope, settings, $state, $http, $q, UtilsService) {

        // Set message headers 
        $http.defaults.headers.common["X-User"] = CSDL_KHCNApp.CurrentUser.Id;
        $http.defaults.headers.common["X-ApplicationId"] = CSDL_KHCNApp.CurrentUser.ApplicationId;

        $rootScope.UserInfo = CSDL_KHCNApp.CurrentUser;

        $rootScope.$state = $state;
        $rootScope.$settings = settings;
        $rootScope.$on('$locationChangeStart', function (event, next, prev) {
            if (CSDL_KHCNApp.Mode == "public" && !next.includes("#!/public")) {
                location.reload();
            }
            if (next.includes("#!/public") && CSDL_KHCNApp.Mode != "public") {
                location.reload();
            }
        });
        $rootScope.$on("$locationChangeSuccess", function (event, next, current) {
            Layout.setSidebarMenuActiveLink("match");
        });
    }]);

    String.prototype.format = function () {
        var a = this;
        for (var k in arguments) {
            a = a.replace("{" + k + "}", arguments[k])
        }
        return a
    }

    //#region Init template
    sidebarModule.init(CSDL_KHCNApp);
    //#endregion
    console.log("%cDừng lại ngay!.", "background: red; color: yellow; font-size: x-large");
    console.log("%cViệc chỉnh sửa các thành phần của ứng dụng có thể gây hại cho hoạt động ứng dụng.", "  color: blue; font-size: large");
    return angularAMD.bootstrap(CSDL_KHCNApp);
});