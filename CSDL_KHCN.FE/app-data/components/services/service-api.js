 define(['angularAMD'], function (angularAMD) {
     'use strict';
     /**API tham số hệ thống*/
     angularAMD.service('ParameterApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
         var service = {};
         var basedUrl = ConstantsApp.BASED_API_URL;
         var prefixbasedUrl = ConstantsApp.API_PARAMETER_URL;
         // #region
         /**Thêm mới */
         service.Create = function (model) {
             var promise = $http({
                 method: 'POST',
                 url: basedUrl + prefixbasedUrl,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         /**Cập nhật */
         service.Update = function (id, model) {
             var promise = $http({
                 method: 'PUT',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         /**Xóa */
         service.Delete = function (id) {
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Xóa nhiều */
         service.DeleteMany = function (listId) {
             var deleteQuery = "";
             for (var i = 0; i < listId.length; i++) {
                 var id = listId[i];
                 if (i === 0) {
                     deleteQuery += "?listId=" + id
                 } else {
                     deleteQuery += "&listId=" + id
                 }
             }
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + deleteQuery,
                 ignoreLoadingBar: false,
                 headers: {
                     'Content-type': ' application/json'
                 },
             });
             return promise;
         }
         /**Lấy theo Id */
         service.GetById = function (id) {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy theo bộ lọc */
         service.GetFilter = function (page, size, filterObj, sort) {
             if (!sort) {
                 sort = "+Id";
             }
             var filter = angular.toJson(filterObj);
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '?page=' + page + '&size=' + size + '&filter=' + filter + '&sort=' + sort,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy tất cả */
         service.GetAll = function () {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/all',
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         // #endregion
         return service;
     }]);
     /**API ứng dụng */
     angularAMD.service('ApplicationApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
         var service = {};
         var basedUrl = ConstantsApp.BASED_API_URL;
         var prefixbasedUrl = ConstantsApp.API_APPLICATION_URL;
         // #region
         /**Thêm mới */
         service.Create = function (model) {
             var promise = $http({
                 method: 'POST',
                 url: basedUrl + prefixbasedUrl,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         /**Cập nhật */
         service.Update = function (id, model) {
             var promise = $http({
                 method: 'PUT',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         /**Xóa */
         service.Delete = function (id) {
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Xóa nhiều */
         service.DeleteMany = function (listId) {
             var deleteQuery = "";
             for (var i = 0; i < listId.length; i++) {
                 var id = listId[i];
                 if (i === 0) {
                     deleteQuery += "?listId=" + id
                 } else {
                     deleteQuery += "&listId=" + id
                 }
             }
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + deleteQuery,
                 ignoreLoadingBar: false,
                 headers: {
                     'Content-type': ' application/json'
                 },
             });
             return promise;
         }
         /**Lấy theo Id */
         service.GetById = function (id) {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy theo bộ lọc */
         service.GetFilter = function (page, size, filterObj, sort) {
             if (!sort) {
                 sort = "+Id";
             }
             var filter = angular.toJson(filterObj);
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '?page=' + page + '&size=' + size + '&filter=' + filter + '&sort=' + sort,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy tất cả */
         service.GetAll = function () {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/all',
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         // #endregion
         /**Lấy theo chủ sở hữu */
         service.GetAccessibleByOwner = function () {
             return $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/owner',
                 ignoreLoadingBar: false,
             });
         }
         /**Lấy theo người dùng */
         service.GetAccessibleByUserId = function (userId) {
             return $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/user/' + userId,
                 ignoreLoadingBar: false,
             });
         }
         return service;
     }]);
     /**API nhóm người dùng */
     angularAMD.service('RoleApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
         var service = {};
         var basedUrl = ConstantsApp.BASED_API_URL;
         var prefixbasedUrl = ConstantsApp.API_ROLE_URL;
         // #region
         /**Thêm mới */
         service.Create = function (model) {
             var promise = $http({
                 method: 'POST',
                 url: basedUrl + prefixbasedUrl,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         /**Cập nhật */
         service.Update = function (id, model) {
             var promise = $http({
                 method: 'PUT',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         /**Xóa */
         service.Delete = function (id) {
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Xóa nhiều */
         service.DeleteMany = function (listId) {
             var deleteQuery = "";
             for (var i = 0; i < listId.length; i++) {
                 var id = listId[i];
                 if (i === 0) {
                     deleteQuery += "?listId=" + id
                 } else {
                     deleteQuery += "&listId=" + id
                 }
             }
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + deleteQuery,
                 ignoreLoadingBar: false,
                 headers: {
                     'Content-type': ' application/json'
                 },
             });
             return promise;
         }
         /**Lấy theo Id */
         service.GetById = function (id) {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy theo bộ lọc */
         service.GetFilter = function (page, size, filterObj, sort) {
             if (!sort) {
                 sort = "+Id";
             }
             var filter = angular.toJson(filterObj);
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '?page=' + page + '&size=' + size + '&filter=' + filter + '&sort=' + sort,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy tất cả */
         service.GetAll = function () {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/all',
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         // #endregion
         /**Lấy chi tiết */
         service.GetDetail = function (id, appId) {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/' + id + '/detail?applicationId=' + appId,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy người dùng liên kết*/
         service.GetUserMapRole = function (id, appId) {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/' + id + '/user?applicationId=' + appId,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         
         /**Gán người dùng liên kết*/
         service.AddUserMapRole = function (id, model, appId) {
             var promise = $http({
                 method: 'POST',
                 url: basedUrl + prefixbasedUrl + '/' + id + '/user?applicationId=' + appId,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         

         /**Xóa người dùng liên kết*/
         service.DeleteUserMapRole = function (id, model, appId) {
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + '/' + id + '/user?applicationId=' + appId,
                 ignoreLoadingBar: false,
                 headers: {
                     'Content-type': ' application/json'
                 },
                 data: model
             });
             return promise;
         }
         
         return service;
     }]);
     /**API người dùng */
     /**API người dùng */
     angularAMD.service('UserApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
         var service = {};
         var basedUrl = ConstantsApp.BASED_API_URL;
         var prefixbasedUrl = ConstantsApp.API_USER_URL;
         var jwtbasedUrl = ConstantsApp.API_JWT_URL;

         // #region
         /**Thêm mới */
         service.Create = function (model) {
             var promise = $http({
                 method: 'POST',
                 url: basedUrl + prefixbasedUrl,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         /**Cập nhật mật khẩu*/
         service.ChangePassword = function (id, password) {
             var promise = $http({
                 method: 'PUT',
                 url: basedUrl + prefixbasedUrl + '/' + id + "/changepassword?password=" + password,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Cập nhật */
         service.Update = function (id, model) {
             var promise = $http({
                 method: 'PUT',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         /**Xóa */
         service.Delete = function (id) {
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Xóa nhiều */
         service.DeleteMany = function (listId) {
             var deleteQuery = "";
             for (var i = 0; i < listId.length; i++) {
                 var id = listId[i];
                 if (i === 0) {
                     deleteQuery += "?listId=" + id
                 } else {
                     deleteQuery += "&listId=" + id
                 }
             }
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + deleteQuery,
                 ignoreLoadingBar: false,
                 headers: {
                     'Content-type': ' application/json'
                 },
             });
             return promise;
         }
         /**Lấy theo Id */
         service.GetById = function (id) {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy theo bộ lọc */
         service.GetFilter = function (page, size, filterObj, sort) {
             if (!sort) {
                 sort = "+Id";
             }
             var filter = angular.toJson(filterObj);
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '?page=' + page + '&size=' + size + '&filter=' + filter + '&sort=' + sort,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy tất cả */
         service.GetAll = function () {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/all',
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         // #endregion
         /**Lấy chi tiết */
         service.CheckNameAvailability = function (name) {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/checkname/' + name,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy chi tiết */
         service.GetDetail = function (id, appId) {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/' + id + '/detail?applicationId=' + appId,
                 ignoreLoadingBar: false,
             });
             return promise;
         } 
         /**Lấy quyền liên kết*/
         service.GetRoleMapUser = function (id, appId) {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/' + id + '/role?applicationId=' + appId,
                 ignoreLoadingBar: false,
             });
             return promise;
         } 
         /**Gán nhóm liên kết*/
         service.AddRoleMapUser = function (id, model, appId) {
             var promise = $http({
                 method: 'POST',
                 url: basedUrl + prefixbasedUrl + '/' + id + '/role?applicationId=' + appId,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         } 
         /**Xóa nhóm liên kết*/
         service.DeleteRoleMapUser = function (id, model, appId) {
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + '/' + id + '/role?applicationId=' + appId,
                 ignoreLoadingBar: false,
                 headers: {
                     'Content-type': ' application/json'
                 },
                 data: model
             });
             return promise;
         } 

         /**Đăng nhập hệ thống */
         service.Login = function (userName, passWord) {
             var model = {
                 'username': userName,
                 'password': passWord
             }
             var promise = $http({
                 method: 'POST',
                 url: basedUrl + jwtbasedUrl + '/token',
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         };
         return service;
     }]);

      
     /**API node*/
     angularAMD.service('NodeApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
        var service = {};
        var basedUrl = ConstantsApp.BASED_API_URL;
        var prefixbasedUrl = ConstantsApp.API_NODE_URL;

        // #region

        service.GetPhysicalUploadSrc = function (destinationpath, isAvartar, isMulti) {
            var url = basedUrl + prefixbasedUrl + "/upload/physical/blob";
            if (isMulti){
                url = url + "/many"
            }
            if (destinationpath)
                url += "?destinationPhysicalPath=" + destinationpath + "&isAvartar=" + isAvartar;
            else
                url += "&isAvartar=" + isAvartar;
            return url;
        }
        return service;
    }]);


 
     /**API nhà khoa học*/
     angularAMD.service('NhaKhoaHocApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
         var service = {};
         var basedUrl = ConstantsApp.BASED_API_URL;
         var prefixbasedUrl = ConstantsApp.API_NHAKHOAHOC_URL;
         // #region
         /**Thêm mới */
         service.Create = function (model) {
             var promise = $http({
                 method: 'POST',
                 url: basedUrl + prefixbasedUrl,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         /**Cập nhật */
         service.Update = function (id, model) {
             var promise = $http({
                 method: 'PUT',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         /**Xóa */
         service.Delete = function (id) {
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Xóa nhiều */
         service.DeleteMany = function (listId) {
             var deleteQuery = "";
             for (var i = 0; i < listId.length; i++) {
                 var id = listId[i];
                 if (i === 0) {
                     deleteQuery += "?listId=" + id
                 } else {
                     deleteQuery += "&listId=" + id
                 }
             }
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + deleteQuery,
                 ignoreLoadingBar: false,
                 headers: {
                     'Content-type': ' application/json'
                 },
             });
             return promise;
         }
         /**Lấy theo Id */
         service.GetById = function (id) {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy theo bộ lọc */
         service.GetFilter = function (page, size, filterObj, sort) {
             if (!sort) {
                 sort = "+Id";
             }
             var filter = angular.toJson(filterObj);
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '?page=' + page + '&size=' + size + '&filter=' + filter + '&sort=' + sort,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy tất cả */
         service.GetAll = function () {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/all',
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         // #endregion
         return service;
     }]);

     /**API hội đồng khoa học*/
     angularAMD.service('HoiDongKhoaHocApiService', ['$http', 'ConstantsApp', function ($http, ConstantsApp) {
         var service = {};
         var basedUrl = ConstantsApp.BASED_API_URL;
         var prefixbasedUrl = ConstantsApp.API_HOIDONGKHOAHOC_URL;
         // #region
         /**Thêm mới */
         service.Create = function (model) {
             var promise = $http({
                 method: 'POST',
                 url: basedUrl + prefixbasedUrl,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         /**Cập nhật */
         service.Update = function (id, model) {
             var promise = $http({
                 method: 'PUT',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
                 data: model
             });
             return promise;
         }
         /**Xóa */
         service.Delete = function (id) {
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Xóa nhiều */
         service.DeleteMany = function (listId) {
             var deleteQuery = "";
             for (var i = 0; i < listId.length; i++) {
                 var id = listId[i];
                 if (i === 0) {
                     deleteQuery += "?listId=" + id
                 } else {
                     deleteQuery += "&listId=" + id
                 }
             }
             var promise = $http({
                 method: 'DELETE',
                 url: basedUrl + prefixbasedUrl + deleteQuery,
                 ignoreLoadingBar: false,
                 headers: {
                     'Content-type': ' application/json'
                 },
             });
             return promise;
         }
         /**Lấy theo Id */
         service.GetById = function (id) {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/' + id,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy theo bộ lọc */
         service.GetFilter = function (page, size, filterObj, sort) {
             if (!sort) {
                 sort = "+Id";
             }
             var filter = angular.toJson(filterObj);
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '?page=' + page + '&size=' + size + '&filter=' + filter + '&sort=' + sort,
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         /**Lấy tất cả */
         service.GetAll = function () {
             var promise = $http({
                 method: 'GET',
                 url: basedUrl + prefixbasedUrl + '/all',
                 ignoreLoadingBar: false,
             });
             return promise;
         }
         // #endregion
         return service;
     }]);

 });