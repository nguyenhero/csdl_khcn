define(["angularAMD"], function (angularAMD) {
    'use strict';
    angularAMD.Version = "";
    angularAMD.controller('InfoDialogCtrl', ['$scope', '$uibModalInstance', 'data', 'option',
        function ($scope, $uibModalInstance, data, option) {
            $scope.Title = option.Title;
            $scope.Message = option.Message;
            $scope.ButtonConfirm = option.ButtonConfirm;
            $scope.ButtonCancel = option.ButtonCancel;
            $scope.PopupData = data;
            $scope.cancel = function () {
                $uibModalInstance.dismiss(false);
            }

            $scope.confirm = function () {
                $uibModalInstance.close('confirm');
            }
        }
    ]);

    angularAMD.controller('InputDialogCtrl', ['$scope', '$uibModalInstance', 'data', 'option',
        function ($scope, $uibModalInstance, data, option) {

            $scope.Title = option.Title;
            $scope.Message = option.Message;
            $scope.Type = option.Type;
            $scope.Data = data;
            $scope.ButtonConfirm = option.ButtonConfirm;
            $scope.ButtonCancel = option.ButtonCancel;

            $scope.cancel = function () {
                $uibModalInstance.dismiss(false);
            }

            $scope.confirm = function () {
                $uibModalInstance.close($scope.Data);
            }
        }
    ]);

    angularAMD.controller('ApproveDialogCtrl', ['$scope', '$uibModalInstance', 'data', 'option',
        function ($scope, $uibModalInstance, data, option) {

            $scope.Title = option.Title;
            $scope.Message = option.Message;
            $scope.ButtonConfirm = option.ButtonConfirm;
            $scope.ButtonCancel = option.ButtonCancel;
            $scope.ButtonReject = option.ButtonReject;
            $scope.ButtonSend = option.ButtonSend;
            $scope.PopupData = data;

            $scope.cancel = function () {
                $uibModalInstance.dismiss(false);
            }

            $scope.confirm = function () {
                $uibModalInstance.close('confirm');
            }

            $scope.reject = function () {
                $uibModalInstance.close('reject');
            }
            $scope.send = function () {
                $uibModalInstance.close('send');
            }
        }
    ]);

    angularAMD.controller('ConfirmProcessDialogCtrl', ['$scope', '$uibModalInstance', 'option', 'data',
        function ($scope, $uibModalInstance, option, data) {

            $scope.Form = {
                Title: "",
                Type: ""
            };

            $scope.Workflow = {
                Comment: "",
                ListDocumentId: [],
                WorkflowCommand: "",
                TransitionHistory: []
            };

            $scope.Button = {
                Confirm: {},
                Close: {}
            };

            $scope.Button.Confirm.Click = function () {
                var data = {};
                data.Command = "confirm";
                data.Comment = $scope.Workflow.Comment;
                data.ListDocumentId = $scope.Workflow.ListDocumentId;
                data.WorkflowCommand = $scope.Workflow.WorkflowCommand;
                if (data !== null) {
                    $uibModalInstance.close(data);
                }
            };

            $scope.Button.Close.Click = function () {
                $uibModalInstance.dismiss('cancel');
            };

            var init = function () {
                $scope.Form.Title = data.Message;
                $scope.Workflow.ListDocumentId = data.ListDocumentId;
                $scope.Workflow.WorkflowCommand = data.WorkflowCommand;
                $scope.Form.Type = option.Type;
                if ($scope.Form.Type === 'view-history') $scope.Workflow.TransitionHistory = data.TransitionHistory;
            };

            init();
        }
    ]);

    angularAMD.controller('KeybroadDialogCtrl', ['$scope', '$uibModalInstance', 'data', 'option',
        function ($scope, $uibModalInstance, data, option) {

            $scope.Title = option.Title;
            //$scope.Message = option.Message;
            //$scope.ButtonConfirm = option.ButtonConfirm;
            //$scope.ButtonCancel = option.ButtonCancel;
            //$scope.PopupData = data;
            $scope.Form = {};
            $scope.Form.InputText = '';
            $scope.Form.FullMode = false;

            if (data === 'full') {
                $scope.Form.FullMode = true;
            }

            $(function () {
                var $write = $('#write'),
                    shift = false,
                    capslock = false;

                $('#keyboard li').click(function () {
                    var $this = $(this),
                        character = $this.html(); // If it's a lowercase letter, nothing happens to this variable

                    // Shift keys
                    if ($this.hasClass('left-shift') || $this.hasClass('right-shift')) {
                        $('.letter').toggleClass('uppercase');
                        $('.symbol span').toggle();

                        shift = (shift === true) ? false : true;
                        capslock = false;
                        return false;
                    }

                    // Caps lock
                    if ($this.hasClass('capslock')) {
                        $('.letter').toggleClass('uppercase');
                        capslock = true;
                        return false;
                    }

                    // Delete
                    if ($this.hasClass('delete')) {
                        var html = $write.html();

                        $write.html(html.substr(0, html.length - 1));
                        $scope.Form.InputText = $write.html();
                        return false;
                    }

                    // Special characters
                    if ($this.hasClass('symbol')) character = $('span:visible', $this).html();
                    if ($this.hasClass('space')) character = ' ';
                    if ($this.hasClass('tab')) {
                        character = "";
                        return;
                    };
                    if ($this.hasClass('return')) {
                        $uibModalInstance.close($scope.Form.InputText);
                        return;
                    };

                    // Uppercase letter
                    if ($this.hasClass('uppercase')) character = character.toUpperCase();

                    // Remove shift once a key is clicked.
                    if (shift === true) {
                        $('.symbol span').toggle();
                        if (capslock === false) $('.letter').toggleClass('uppercase');

                        shift = false;
                    }

                    // Add the character
                    $write.html($write.html() + character);
                    $scope.Form.InputText = $write.html();
                });
            });

            // Barcode, QR code Scanner listenner
            $(document).scannerDetection({

                timeBeforeScanTest: 100, // wait for the next character for upto 200ms
                avgTimeByChar: 20, // it's not a barcode if a character takes longer than 100ms
                preventDefault: true,

                endChar: [13],
                onComplete: function (barcode, qty) {

                        $scope.Form.InputText = barcode;
                        $uibModalInstance.close($scope.Form.InputText);

                    } // main callback function	,
                    ,
                onError: function (string, qty) {

                    //$('#userInput').val($('#userInput').val() + string);

                }


            });

            $scope.Calc = {};
            $scope.Calc.Go = function (x) {

                if ($scope.Form.InputText !== '' && $scope.Form.InputText[0] === '0') {
                    return;
                }
                if (x === 'remove') {
                    $scope.Form.InputText = $scope.Form.InputText.substring(0, $scope.Form.InputText.length - 3);
                } else if (x === 'total') {
                    $scope.Form.InputText = $scope.Form.BillData.TotalRound.toString();
                } else {
                    $scope.Form.InputText += x;
                }

            };

            $scope.cancel = function () {
                $uibModalInstance.close(false);
            }

            $scope.confirm = function () {
                $uibModalInstance.close($scope.Form.InputText);
            }
        }
    ]);

    angularAMD.service("UtilsService", ["$uibModal", "$http", "$q", "ConstantsApp", "$rootScope", "$log", function ($uibModal, $http, $q, ConstantsApp, $rootScope, $log) {
        var service = {};
        var basedApiUrl = ConstantsApp.BASED_API_URL;
        service.ValidateResourceUrl = function (url) {
            if (url)
                return url.replace("{{API_STATIC_FILE}}", ConstantsApp.API_STATIC_FILE);
            return "";
        }
        service.OpenDialog = function (message, title, buttonConfirm, buttonCancel, popupSize, popupData) {
            var infoDialogTemplateUrl = '/app-data/views/template/confirm/info-dialog.html' + angularAMD.Version;
            var modalInstance = $uibModal.open({
                templateUrl: infoDialogTemplateUrl,
                controller: 'InfoDialogCtrl',
                size: popupSize,
                backdrop: "static",
                keyboard: false,
                resolve: {
                    data: function () {
                        var obj = popupData;
                        return obj;
                    },
                    option: function () {
                        return {
                            Message: message,
                            Title: title,
                            ButtonConfirm: buttonConfirm,
                            ButtonCancel: buttonCancel
                        };
                    }
                }
            });

            return modalInstance;
        };

        service.OpenInputDialog = function (message, title, buttonConfirm, buttonCancel, popupSize, type, presetData) {
            var dialogUrl = '/app-data/views/template/confirm/input-dialog.html' + angularAMD.Version;
            var modalInstance = $uibModal.open({
                templateUrl: dialogUrl,
                controller: 'InputDialogCtrl',
                size: popupSize,
                backdrop: "static",
                keyboard: false,
                resolve: {
                    data: function () {
                        var obj = presetData;
                        return obj;
                    },
                    option: function () {
                        return {

                            Message: message,
                            Type: type,
                            Title: title,
                            ButtonConfirm: buttonConfirm,
                            ButtonCancel: buttonCancel
                        };
                    }
                }
            });

            return modalInstance;
        };
        service.OpenKeyBoard = function (type, popupSize, title) {
            var infoDialogTemplateUrl = '/app-data/views/template/touchkeybroad/keyboard.html' + angularAMD.Version;
            var modalInstance = $uibModal.open({
                templateUrl: infoDialogTemplateUrl,
                controller: 'KeybroadDialogCtrl',
                size: popupSize,
                backdrop: "static",
                keyboard: false,
                resolve: {
                    data: function () {
                        var obj = type;
                        return obj;
                    },
                    option: function () {
                        return {
                            Title: title
                        };
                    }
                }
            });

            return modalInstance;
        };
        service.OpenApproveDialog = function (message, title, buttonSend, buttonConfirm, buttonReject, buttonCancel, popupSize, popupData) {
            var infoDialogTemplateUrl = '/app-data/views/template/confirm/approve-dialog.html' + angularAMD.Version;
            var modalInstance = $uibModal.open({
                templateUrl: infoDialogTemplateUrl,
                controller: 'ApproveDialogCtrl',
                size: popupSize,
                backdrop: "static",
                keyboard: false,
                resolve: {
                    data: function () {
                        var obj = popupData;
                        return obj;
                    },
                    option: function () {
                        return {
                            Message: message,
                            Title: title,
                            ButtonConfirm: buttonConfirm,
                            ButtonReject: buttonReject,
                            ButtonCancel: buttonCancel,
                            ButtonSend: buttonSend
                        };
                    }
                }
            });

            return modalInstance;
        };

        service.RemoveVietNamSign = function (obj) {
            var str;
            //if (eval(obj))
            //    str = eval(obj).value;
            //else
            str = obj;
            str = str.toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            str = str.replace(/̣|̉|̀|̃|́|/g, "");
            str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");
            /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
            str = str.replace(/-+-/g, "-");
            //thay thế 2- thành 1- 
            str = str.replace(/^\-+|\-+$/g, "");
            //cắt bỏ ký tự - ở đầu và cuối chuỗi 
            return str.toLowerCase();
        };
        service.AddDays = function (date, days, isDateTime, isMin) {
            var newdate = new Date(date);
            if (isDateTime !== null) {
                // Return Date only
                if (!isDateTime && isMin !== null && isMin) newdate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
                if (!isDateTime && isMin !== null && !isMin) newdate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 99);
            }

            return newdate.setDate(date.getDate() + days);
        };
        // Returns an array of dates between the two dates
        service.GetListDatesBetween = function (startDate, endDate) {
            var dates = [],
                currentDate = startDate,
                addDays = function (days) {
                    var date = new Date(this.valueOf());
                    date.setDate(date.getDate() + days);
                    return date;
                };
            while (currentDate <= endDate) {
                dates.push(currentDate);
                currentDate = addDays.call(currentDate, 1);
            }
            return dates;
        };
        service.NumberOnly = function (obj) {
            var str;
            //if (eval(obj))
            //    str = eval(obj).value;
            //else
            str = obj;

            str = str.toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "");
            str = str.replace(/đ/g, "");
            str = str.replace(/q|w|e|r|t|y|u|i|o|p|a|s|d|f|g|h|j|k|l|z|x|c|v|b|n|m/g, "");
            str = str.replace(/̣|̉|̀|̃|́|/g, "");
            str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "");
            /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
            str = str.replace(/-+-/g, "");
            //thay thế 2- thành 1- 
            str = str.replace(/\s+/g, '');
            str = str.replace(/^\-+|\-+$/g, "");
            //cắt bỏ ký tự - ở đầu và cuối chuỗi 
            str = str.trim();
            return str.toLowerCase();
        };
        service.ConvertToCurrency = function (num) {
            num = num.replace(/,/g, "")
            //num = parseLong(num);
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            //return num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            //return num.toLocaleString('en-VI');
        };
        service.ConvertTimeSpanToSec = function (timeSpan) {
            var sec = 0;
            var inputArr = timeSpan.split(':');
            for (var i = 0; i < inputArr.length; i++) {
                if (i === 0) sec += parseInt(inputArr[0]) * 3600;
                if (i === 1) sec += parseInt(inputArr[1]) * 60;
                if (i === 2) sec += parseInt(inputArr[1]);
            }
            return sec;
        };
        service.ConvertToLocalDate = function (dateValue) {
            //Convert từ Datetime C# sang Datetime Javascript
            var d = new Date(dateValue);
            //Bù giờ từ Datetime C# (GMT) sang Local Datetime trên browser
            var local = d.getTime() - (d.getTimezoneOffset() * 60000);
            //Trả về giờ đã convert sang Local Datetime
            return new Date(local);
        };
        service.ConvertToCurrencyByLocale = function (num, locale) {
            var str = new String(num);
            str = str.replace(/\./g, "");
            var number = Math.round(str);
            if (Number.isNaN(number)) {
                number = 0;
            }
            return number.toLocaleString(locale);
        };
        service.CreateGUID = function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        }


        return service;
    }]);
});