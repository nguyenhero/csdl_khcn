define([], function () {
    return {
        "ENV": "dev",
        "FE_URL": "http://localhost:5002",
        "API_URL": "http://localhost:5000",
        "STATIC_FILE_URL": "http://localhost:5000/StaticFiles/",
        "AUTH_MODE": "JWT",
        "AUTH_WSO2_URI": "https://10.10.100.145:9443/",
        "AUTH_WSO2_CLIENTID": "veGKuuvb8jA91qfEOJNI_YW9Fnga",

    }
});