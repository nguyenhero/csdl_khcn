/* SAVIS Vietnam Corporation
 *
 * This module is to separate configuration of routing to app.js
 * DuongPD - Jan 2018
 */

define(function () {
    'use strict';

    var basedUrl = "/app-data/";

    /* App version */
    var version = "";
    var factory = {};

    /* Setup essential routing for site select */
    factory.setupSiteAccessConfig = function (app) {
        // Route config
        app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/login");
            $stateProvider
                .state('login', {
                    url: "/login",
                    templateUrl: basedUrl + "views/template/login/login.html",
                    data: {
                        pageTitle: 'Đăng nhập hệ thống'
                    },
                    controller: "LoginCtrl",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'CSDL_KHCNApp',
                                files: [
                                    'app-data/views/template/login/login.js',
                                ]
                            });
                        }]
                    }
                })
        }]);
    }; 
    factory.setupShareConfig = function (app) {
        // Route config
        app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise("/public");
            $stateProvider.state('public', {
                url: "/public",
                templateUrl: basedUrl + "views/template/page-public/page-public.html" + version,
                data: {
                    pageTitle: 'Không tìm thấy trang'
                },
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'CSDL_KHCNApp',
                            files: [
                                'app-data/views/template/page-public/page-public.css' + version,
                                'app-data/views/template/page-public/page-public.js' + version
                            ]
                        });
                    }]
                }
            })
             
        }]);
    };
    /* Setup all routings */
    factory.setupRouteConfig = function (app) {
        version = app.Version;
        // console.log(version);
        /* Setup Rounting For All Pages */
        app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise("/users");
            $stateProvider
                // #region System core 
                .state('roles', {
                    url: "/roles",
                    templateUrl: basedUrl + "views/core/role/role-list.html" + version,
                    data: {
                        pageTitle: 'Quản lý nhóm người dùng'
                    },
                    controller: "RoleController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'CSDL_KHCNApp',
                                files: [
                                    'app-data/views/core/role/role-list.js' + version,
                                ]
                            });
                        }]
                    }
                })
                .state('users', {
                    url: "/users",
                    templateUrl: basedUrl + "views/core/user/user.html" + version,
                    data: {
                        pageTitle: 'Quản lý người dùng'
                    },
                    controller: "UserController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'CSDL_KHCNApp',
                                files: [
                                    'node_modules/ng-img-crop/compile/minified/ng-img-crop.js',
                                    'node_modules/ng-img-crop/compile/minified/ng-img-crop.css',
                                    'app-data/views/core/user/user.js' + version
                                    
                                ]
                            });
                        }]
                    }
                })
                .state('nha-khoa-hoc', {
                    url: "/nha-khoa-hoc",
                    templateUrl: basedUrl + "views/bus/nha-khoa-hoc/nha-khoa-hoc-list.html" + version,
                    data: {
                        pageTitle: 'Quản lý nhà khoa học'
                    },
                    controller: "NhaKhoaHocController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'CSDL_KHCNApp',
                                files: [
                                    'app-data/views/bus/nha-khoa-hoc/nha-khoa-hoc-list.js' + version,
                                ]
                            });
                        }]
                    }
                })
                .state('hoi-dong-khoa-hoc', {
                    url: "/hoi-dong-khoa-hoc",
                    templateUrl: basedUrl + "views/bus/hoi-dong-khoa-hoc/hoi-dong-khoa-hoc-list.html" + version,
                    data: {
                        pageTitle: 'Quản lý hội đồng khoa học'
                    },
                    controller: "HoiDongKhoaHocController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'CSDL_KHCNApp',
                                files: [
                                    'app-data/views/bus/hoi-dong-khoa-hoc/hoi-dong-khoa-hoc-list.js' + version,
                                ]
                            });
                        }]
                    }
                })
                .state('404', {
                    url: "/404",
                    templateUrl: basedUrl + "views/template/page-error/page-error.html" + version,
                    data: {
                        pageTitle: 'Không tìm thấy trang'
                    },
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'CSDL_KHCNApp',
                                files: [
                                    'app-data/views/template/page-error/page-error.css' + version,
                                    'app-data/views/template/page-error/page-error.js' + version
                                ]
                            });
                        }]
                    }
                })
                .state('public', {
                    url: "/public",
                    templateUrl: basedUrl + "views/template/page-public/page-public.html" + version,
                    data: {
                        pageTitle: 'Không tìm thấy trang'
                    },
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'CSDL_KHCNApp',
                                files: [
                                    'app-data/views/template/page-public/page-public.css' + version,
                                    'app-data/views/template/page-public/page-public.js' + version
                                ]
                            });
                        }]
                    }
                })

        }]);
    };

    return factory;
});