! function () {
    'use strict';
    var app = angular.module("CSDL_KHCNApp");
    app.controller('LoginCtrl', ['$scope', '$log', '$location', 'UserApiService',
        function ($scope, $log, $location, UserApiService) {
            /* REGION : LOADING STATES */
            $scope.SignIn = function () {
                var promise = UserApiService.Login($scope.UserName, $scope.Password);
                promise.then(function onSuccess(response) {
                    var dataResult = response.data;
                    app.User = response.data;
                    //var path = $location.host();
                    window.localStorage['user_id'] = dataResult.data.userId;
                    window.localStorage['display_name'] = dataResult.data.userModel.name;
                    window.localStorage['app_id'] = dataResult.data.applicationId;
                    window.localStorage['access_token'] = dataResult.data.tokenString;
                    window.localStorage['expires_time'] = dataResult.data.timeExpride;
                    $location.path('/home');
                    location.reload();
                }, function onError(response) {
                    $log.error(response);
                });
                return promise;
            };
            //todo: form signup
        }
    ]);
}()