define(['angularAMD'], function (angularAMD) {
    'use strict';
    var sidebar = {};

    sidebar.init = function (app) {
        app.controller('SidebarController', ['$timeout',
            function ($timeout) {
                $timeout(function(){
                    Layout.initSidebar(); // init sidebar
                },500);
            }
        ]);
    };

    return sidebar;
});