! function () {
    'use strict';
    var app = angular.module("CSDL_KHCNApp");
    app.controller('RoleController', ['$scope', '$location', '$filter', '$log', '$uibModal',
        '$q', 'ConstantsApp', 'UtilsService', 'toastr', 'RoleApiService',
        function ($scope, $location, $filter, $log, $uibModal, $q, ConstantsApp, UtilsService, $notifications, RoleApiService) {
            /* Declare variables url form popup add,edit,info */
            var itemDialogTemplateUrl = '/app-data/views/core/role/role-modal-item.html' + app.Version;
            // Lấy id người dùng hiện tại
            var currentUserId = app.CurrentUser.Id;
            /* ------------------------------------------------------------------------------- */
            /* FORM */
            /* ------------------------------------------------------------------------------- */
            $scope.Form = {};
            $scope.Form.CurrentUserId = currentUserId;

            $scope.FormMode = {};
            $scope.FormMode.IsReadMode = false;
            $scope.FormMode.IsAllowDelete = false;

            /* ------------------------------------------------------------------------------- */


            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};
            // Button ADD
            $scope.Button.Add = {
                Enable: true,
                Visible: true,
                Title: "Thêm",
                GrantAccess: true
            };
            $scope.Button.Add.Click = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'RoleModalController',
                    size: 'md',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            return obj;
                        },
                        option: function () {
                            return [{
                                Type: 'add'
                            }];
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    $notifications.success('Thêm thành công', 'Thông báo');
                    initData();
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button UPDATE
            $scope.Button.Update = {
                Enable: true,
                Visible: true,
                Title: "Cập nhật",
                GrantAccess: true
            };
            $scope.Button.Update.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'RoleModalController',
                    size: 'md',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            obj = item;
                            return obj;
                        },
                        option: function () {
                            return [{
                                Type: 'edit'
                            }];
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    $notifications.success('Cập nhật thành công');
                    $scope.Button.Delete.Visible = false;
                    initData();
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };


            // Button DELETE
            $scope.Button.Delete = {
                Enable: true,
                Visible: false,
                Title: "Xóa",
                GrantAccess: true
            };
            $scope.Button.Delete.Click = function (item) {
                var listItemDeleteId = [];
                var count = 0;
                if (item !== null && typeof item !== 'undefined') {
                    listItemDeleteId.push(item.id);
                    count++;
                } else {
                    for (var i = 0; i < $scope.Grid.Data.length; i++) {

                        if ($scope.Grid.Data[i].selected === true) {
                            listItemDeleteId.push($scope.Grid.Data[i].id);
                            count++;
                        }
                    }
                }

                var message = "";
                if (count > 1) {
                    message = "Bạn có chắc muốn xóa những nhóm người dùng này này ?";
                } else {
                    message = "Bạn có chắc muốn xóa nhóm người dùng này ?";
                }
                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);

                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {
                        var promise = RoleApiService.DeleteMany(listItemDeleteId);
                        promise.then(function onSuccess(response) {
                            if (dataResult.code == 200) {
                                $scope.Grid.Refesh();
                                $notifications.success("Xóa thành công!");
                            } else {
                                $log.error("ERROR", response);
                                UtilsService.OpenDialog('Kết quả', 'Thông báo: ' + response.data.message, '', 'Đóng', 'md');
                            }
                        }, function onError(response) {
                            $log.error("ERROR", response);
                            UtilsService.OpenDialog('Kết quả', 'Thông báo: ' + response.data.message, '', 'Đóng', 'md');
                        });
                    }
                });
                /* Check all item is selected => Request to API  */
            };

            // Button GOTOPAGE
            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function (pageTogo) {
                $location.search("pn", pageTogo);
                $location.search("ts", $scope.Filter.Text);
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            // Button GOTOPAGE
            $scope.Button.ChangePageSize = {};
            $scope.Button.ChangePageSize.Click = function () {
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            /* ------------------------------------------------------------------------------- */

            /* DROPDOWNLIST FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.DropDownList = {};

            /* ------------------------------------------------------------------------------- */

            /* FILTERs */
            /* ------------------------------------------------------------------------------- */
            $scope.Filter = {};
            $scope.Filter.Text = "";

            /* ------------------------------------------------------------------------------- */

            /* GRIDVIEWs */
            /* ------------------------------------------------------------------------------- */
            $scope.Grid = ConstantsApp.GRID_ATTRIBUTES;

            $scope.Grid.DataHeader = [{
                    Key: 'code',
                    Value: "Mã nhóm",
                    Width: 'auto',
                    IsSortable: false
                },
                {
                    Key: 'name',
                    Value: "Tên nhóm",
                    Width: 'auto',
                    IsSortable: false
                },
                {
                    Key: 'description',
                    Value: "Ghi chú",
                    Width: 'auto',
                    IsSortable: false
                },
                {
                    Key: '#',
                    Value: "Thao tác",
                    Width: '60px',
                    IsSortable: false
                }
            ];

            $scope.Grid.Search = function () {
                $location.search("pn", 1);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.Grid.Order = function (predicate, reverse) {
                $scope.Grid.Data = orderBy($scope.Grid.Data, predicate, reverse);
            };
            /* This function is to Check all item */
            $scope.Grid.CheckAllItem = function () {
                $scope.Grid.CheckAll = !$scope.Grid.CheckAll;

                angular.forEach($scope.Grid.Data, function (item) {
                    item.selected = $scope.Grid.Check;
                });

                // If list has more than 1 item
                if ($scope.Grid.CheckAll && $scope.Grid.Data.length > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                }

                // If list has 1 item
                if ($scope.Grid.CheckAll && $scope.Grid.Data.length === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                }

                if (!$scope.Grid.Check) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = false;
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemClass = function (item) {
                if (item.selected === true) {
                    return 'selected';
                } else {
                    return '';
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemHeaderClass = function (item) {
                if (item.selected === true) {
                    return 'sorting_asc';
                } else if (item.selected === false) {
                    return 'sorting_desc';
                } else if (!item.IsSortable) {
                    return '';
                } else {
                    return 'sorting';
                }
            };
            /* Sorting grid By Click to header */
            $scope.Grid.ClickToHeader = function (item) {
                if (item.IsSortable) {
                    // Set all class header to default
                    angular.forEach($scope.dataHeader, function (items) {
                        if (items !== item) {
                            items.selected = null;
                        }
                    });
                    // Set for item click sorted
                    if (item.selected === true) {
                        item.selected = false;
                        $scope.Grid.Order(item.Key, false);
                    } else {
                        item.selected = true;
                        $scope.Grid.Order("-" + item.Key, false);
                    }
                }
            };
            /* This function is to Check items in grid*/
            $scope.Grid.CheckItem = function (item) {
                item.selected = !item.selected;
                var count = 0;
                // Calculate item selected count     
                angular.forEach($scope.Grid.Data, function (item) {
                    if (item.selected === true) {
                        count++;
                    }
                });

                // Reset all buttons
                $scope.Button.Update.Visible = false;
                $scope.Button.Delete.Visible = false;
                $scope.Grid.CheckAll = false;

                if (count === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }

                if (count > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }
                // If all items is checked
                if (count > 0 && count === $scope.Grid.Data.length) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = true;
                    $scope.Grid.CheckAll = true;
                }
            };
            /* This function click refesh grid program */
            $scope.Grid.Refesh = function () {
                $scope.Grid.PageNumber = 1;
                $scope.Filter.Text = "";
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            /* ------------------------------------------------------------------------------- */

            /* INIT FUNCTIONS */
            /* ------------------------------------------------------------------------------- */

            // Init nút chức năng theo quyền người dùng
            var initButtonByRightOfUser = function () {
                //$scope.Button.Add.GrantAccess = UtilsService.CheckRightOfUser("ROLE-ADD");
                //$scope.Button.Update.GrantAccess = UtilsService.CheckRightOfUser("ROLE-UPDATE");
                //$scope.Button.Delete.GrantAccess = UtilsService.CheckRightOfUser("ROLE-DELETE");
                $scope.Button.Add.GrantAccess = true;
                $scope.Button.Update.GrantAccess = true;
                $scope.Button.Delete.GrantAccess = true;
                return true;
            };

            /* Load data from API binding to Grid*/
            var initData = function () {
                $scope.Grid.CheckAll = false;
                $scope.Button.Delete.Visible = false;
                $scope.Grid.Data.forEach(function (items) {
                    items.selected = null;
                });
                $scope.Grid.CheckAll = null;

                var qs = $location.search();
                /* PageNumber */
                if (typeof (qs["pn"]) !== 'undefined') {
                    $scope.Grid.PageNumber = parseInt(qs["pn"]);
                } else {
                    // 
                    $location.search("pn", "1");
                    $scope.Grid.PageNumber = 1;
                };
                /* PageSize */
                if (typeof (qs["ps"]) !== 'undefined') {
                    $scope.Grid.PageSize = parseInt(qs["ps"]);
                } else {
                    // 
                    $location.search("ps", "10");
                    $scope.Grid.PageSize = 10;
                };
                /* TextSearch */
                if (typeof (qs["ts"]) !== 'undefined') {
                    $scope.Filter.Text = qs["ts"];
                } else {
                    // 
                    $location.search("ts", "");
                    $scope.Filter.Text = "";
                };

                var page = $scope.Grid.PageNumber;
                var size = $scope.Grid.PageSize;
                var filterObj = {
                    "fullTextSearch": $scope.Filter.Text
                }
                var sort = "";

                var promise = RoleApiService.GetFilter(page, size, filterObj, sort);
                promise.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.code == 200) {
                        var dataResult = response.data;
                        $scope.Grid.Data = dataResult.data.content;
                        $scope.Grid.TotalCount = dataResult.data.totalElements;
                        $scope.Grid.TotalPage = dataResult.data.totalPages;
                        $scope.Grid.FromRecord = dataResult.data.size * (dataResult.data.page - 1);
                        $scope.Grid.ToRecord = $scope.Grid.FromRecord + dataResult.data.numberOfElements;
                    } else {
                        $log.error("ERROR", response);
                        UtilsService.OpenDialog('Kết quả', 'Thông báo: ' + response.data.message, '', 'Đóng', 'md');
                    }
                }, function onError(response) {
                    $log.error("ERROR", response);
                    UtilsService.OpenDialog('Kết quả', 'Thông báo: ' + response.data.message, '', 'Đóng', 'md');
                });
                return promise;
            };

            var initMain = function () {
                $q.all([initButtonByRightOfUser()]).then(function () {
                    initData();
                });
            };

            initMain();

            /* ------------------------------------------------------------------------------- */
        }
    ]);
    /* Controller for froms Popup Add,Edit,Delete or Info */
    app.controller("RoleModalController", ["$scope", "$q", "$uibModalInstance", '$log',
        "item", "option", "toastr", "RoleApiService",
        function ($scope, $q, $uibModalInstance, $log, item, option, $notifications, RoleApiService) {
            $scope.Form = {};
            $scope.Form.Title = "";
            $scope.Form.CurrentUserId = app.CurrentUser.Id;
            $scope.Form.ApplicationId = app.CurrentUser.ApplicationId;
            $scope.Form.Item = angular.copy(item);
            $scope.Form.Option = option;
            $scope.Button = {};
            $scope.Button.Close = {};
            $scope.Button.Close.Click = function () {
                $uibModalInstance.dismiss('cancel');
            };

            /* DropDownList */
            /* ------------------------------------------------------------------------------- */
            $scope.DropDownList = {};

            /* ------------------------------------------------------------------------------- */

            /* CHECKBOX FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.CheckBox = {};

            /* ------------------------------------------------------------------------------- */

            $scope.Autofocus = false;

            /* Check parameters type = 'edit' to load data edit to form edit */

            /* Function button "Save" with add and edit button*/
            $scope.Save = function () {

                if ($scope.Form.Option[0].Type === 'add') {
                    var postData = $scope.Form.Item;
                    var promise = RoleApiService.Create(postData);
                    promise.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.code == 200) {
                            var dataResult = response.data;
                            $uibModalInstance.close(dataResult);
                        } else {
                            $log.error("ERROR", response);
                            UtilsService.OpenDialog('Kết quả', 'Chú ý', 'Thông báo : ' + response.data.message, 'Đóng', 'md');
                        }
                    }, function onError(response) {
                        $log.error("ERROR", response);
                        UtilsService.OpenDialog('Kết quả', 'Chú ý', 'Thông báo : ' + response.data.message, 'Đóng', 'md');
                    });
                    return promise;
                } else {
                    var putData = $scope.Form.Item;
                    promise = RoleApiService.Update(putData.id, putData);
                    promise.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.code == 200) {
                            var dataResult = response.data;
                            $uibModalInstance.close(dataResult);
                        } else {
                            $log.error("ERROR", response);
                            UtilsService.OpenDialog('Kết quả', 'Chú ý', 'Thông báo : ' + response.data.message, 'Đóng', 'md');
                        }
                    }, function onError(response) {
                        $log.error("ERROR", response);
                        UtilsService.OpenDialog('Kết quả', 'Chú ý', 'Thông báo : ' + response.data.message, 'Đóng', 'md');
                    });
                    return promise;
                }
            };

            var groupBy = function (xs, key) {
                return xs.reduce(function (rv, x) {
                    (rv[x[key]] = rv[x[key]] || []).push(x);
                    return rv;
                }, {});
            };



            var initMain = function () {
                if ($scope.Form.Option[0].Type === 'edit') {
                    $scope.Form.Title = "Cập nhật nhóm người dùng";
                } /* Check parameters type = 'info' to load data info to form info */
                else if ($scope.Form.Option[0].Type === 'add') {
                    $scope.Form.Title = "Thêm nhóm người dùng";
                };
            };

            initMain(); 
        }
    ]);
}();