! function () {
    'use strict';
    var app = angular.module("CSDL_KHCNApp");
    app.controller('NhaKhoaHocController', ['$scope', '$location', '$filter', '$http', '$uibModal',
        '$routeParams', '$log', '$q', 'UtilsService', 'toastr', 'NhaKhoaHocApiService', 'ConstantsApp',
        function ($scope, $location, $filter, $http, $uibModal, $routeParams, $log, $q, UtilsService, $notifications,
            NhaKhoaHocApiService, ConstantsApp) {
            /* Declare variables url form popup add,edit,info */
            var itemDialogTemplateUrl = '/app-data/views/bus/nha-khoa-hoc/nha-khoa-hoc-item.html' + app.Version;

            /* FORM */
            /* ------------------------------------------------------------------------------- */
            $scope.Form = {};
            $scope.FormMode = {};
            $scope.FormMode.IsReadMode = false;
            $scope.FormMode.IsAllowDelete = false;
            /* ------------------------------------------------------------------------------- */
            /* SELECTED */
            /* ------------------------------------------------------------------------------- */
            $scope.Selected = {};
            $scope.Selected.LinhVucList = ConstantsApp.LinhVucList;
            $scope.Selected.ChucDanhNghienCuuList = ConstantsApp.ChucDanhNghienCuuList;


            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};
            // Button DownloadAttactment
            $scope.Button.DownloadAttactment = {
                Enable: true,
                Visible: true,
                Title: "Thêm",
                GrantAccess: true
            };
            $scope.Button.DownloadAttactment.Click = function () {

            };
            // Button ADD
            $scope.Button.Add = {
                Enable: true,
                Visible: true,
                Title: "Thêm",
                GrantAccess: true
            };
            $scope.Button.Add.Click = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'NhaKhoaHocModalController',
                    size: 'lg',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            return obj;
                        },
                        option: function () {
                            return [{
                                Type: 'add'
                            }];
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    $notifications.success('Thêm thành công', 'Thông báo');
                    initData();
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button UPDATE
            $scope.Button.Update = {
                Enable: true,
                Visible: true,
                Title: "Cập nhật",
                GrantAccess: true
            };
            $scope.Button.Update.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'NhaKhoaHocModalController',
                    size: 'lg',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            obj = item;
                            return obj;
                        },
                        option: function () {
                            return [{
                                Type: 'edit'
                            }];
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    $notifications.success('Cập nhật thành công');
                    $scope.Button.Delete.Visible = false;
                    initData();
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };


            // Button DELETE
            $scope.Button.Delete = {
                Enable: true,
                Visible: false,
                Title: "Xóa",
                GrantAccess: true
            };
            $scope.Button.Delete.Click = function (item) {
                var listItemDeleteId = [];
                var count = 0;
                if (item !== null && typeof item !== 'undefined') {
                    listItemDeleteId.push(item.id);
                    count++;
                } else {
                    for (var i = 0; i < $scope.Grid.Data.length; i++) {

                        if ($scope.Grid.Data[i].selected === true) {
                            listItemDeleteId.push($scope.Grid.Data[i].id);
                            count++;
                        }
                    }
                }

                var message = "";
                if (count > 1) {
                    message = "Bạn có chắc muốn xóa những nhà khoa học này ?";
                } else {
                    message = "Bạn có chắc muốn xóa nhà khoa học này ?";
                }
                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);

                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {
                        var promise = NhaKhoaHocApiService.DeleteMany(listItemDeleteId);
                        promise.then(function onSuccess(response) {
                            var dataResult = response.data;
                            if (dataResult.code == 200) {
                                $scope.Grid.Refresh();
                                $notifications.success("Xóa thành công!");
                            } else {
                                $log.error("ERROR", response);
                                UtilsService.OpenDialog('Kết quả', 'Thông báo: ' + response.data.message, '', 'Đóng', 'md');
                            }
                        }, function onError(response) {
                            $log.error("ERROR", response);
                            UtilsService.OpenDialog('Kết quả', 'Thông báo: ' + response.data.message, '', 'Đóng', 'md');
                        });
                    }
                });
                /* Check all item is selected => Request to API  */
            };

            // Button GOTOPAGE
            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function (pageTogo) {
                $location.search("pn", pageTogo);
                $location.search("ts", $scope.Filter.Text);
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            // Button GOTOPAGE
            $scope.Button.ChangePageSize = {};
            $scope.Button.ChangePageSize.Click = function () {
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            /* ------------------------------------------------------------------------------- */

            /* DROPDOWNLIST FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.DropDownList = {};

            /* ------------------------------------------------------------------------------- */

            /* FILTERs */
            /* ------------------------------------------------------------------------------- */
            $scope.Filter = {};
            $scope.Filter.Text = "";

            /* ------------------------------------------------------------------------------- */

            /* GRIDVIEWs */
            /* ------------------------------------------------------------------------------- */
            $scope.Grid = ConstantsApp.GRID_ATTRIBUTES;

            $scope.Grid.DataHeader = [{
                Key: 'hoTen',
                Value: "Họ và Tên",
                Width: "150px"
            },
            {
                Key: 'namSinh',
                Value: "Năm sinh",
                Width: "65px"
            },
            {
                Key: 'gioiTinh',
                Value: "Giới tính",
                Width: "60px"
            },
            {
                Key: 'linhVuc',
                Value: "Lĩnh vực NC",
                Width: "auto"
            },
            {
                Key: 'chucVuHienNay',
                Value: "Chức vụ hiện nay",
                Width: "150px"
            },
            {
                Key: 'tenCoQuan',
                Value: "Tên cơ quan c.tác",
                Width: "150px"
            },
            {
                Key: 'chucDanhNghienCuu',
                Value: "Chức danh NC",
                Width: "150px"
            },
            {
                Key: '#',
                Value: "Thao tác",
                Width: '60px',
                IsNotSortable: true
            }
            ];

            $scope.Grid.Search = function () {

                $location.search("pn", 1);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("ts", $scope.Filter.Text);
                if ($scope.Selected.LinhVuc) {
                    $location.search("lv", $scope.Selected.LinhVuc);
                } else {
                    $location.search("lv", null);
                }
                if ($scope.Selected.ChucDanhNghienCuu) {
                    $location.search("cdnc", $scope.Selected.ChucDanhNghienCuu);
                } else {
                    $location.search("cdnc", null);
                }
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.Grid.Order = function (predicate, reverse) {
                $scope.Grid.Data = orderBy($scope.Grid.Data, predicate, reverse);
            };
            /* This function is to Check all item */
            $scope.Grid.CheckAllItem = function () {
                $scope.Grid.CheckAll = !$scope.Grid.CheckAll;

                angular.forEach($scope.Grid.Data, function (item) {
                    item.selected = $scope.Grid.Check;
                });

                // If list has more than 1 item
                if ($scope.Grid.CheckAll && $scope.Grid.Data.length > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                }

                // If list has 1 item
                if ($scope.Grid.CheckAll && $scope.Grid.Data.length === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                }

                if (!$scope.Grid.Check) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = false;
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemClass = function (item) {

                if (item.selected === true) {
                    return 'selected';
                } else {
                    return '';
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemHeaderClass = function (item) {
                if (item.selected === true) {
                    return 'sorting_asc';
                } else if (item.selected === false) {
                    return 'sorting_desc';
                } else if (item.IsNotSortable) {
                    return '';
                } else {
                    return 'sorting';
                }
            };
            /* Sorting grid By Click to header */
            $scope.Grid.ClickToHeader = function (item) {
                if (!item.IsNotSortable) {
                    // Set all class header to default
                    angular.forEach($scope.Grid.DataHeader, function (items) {
                        if (items !== item) {
                            items.selected = null;
                        }
                    });
                    // Set for item click sorted
                    if (item.selected === true) {
                        item.selected = false;
                        $scope.Grid.Order(item.Key, false);
                    } else {
                        item.selected = true;
                        $scope.Grid.Order("-" + item.Key, false);
                    }
                }
            };
            /* This function is to Check items in grid*/
            $scope.Grid.CheckItem = function (item) {
                item.selected = !item.selected;
                var count = 0;
                // Calculate item selected count     
                angular.forEach($scope.Grid.Data, function (item) {
                    if (item.selected === true) {
                        count++;
                    }
                });

                // Reset all buttons
                $scope.Button.Update.Visible = false;
                $scope.Button.Delete.Visible = false;
                $scope.Grid.CheckAll = false;

                if (count === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }

                if (count > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }
                // If all items is checked
                if (count > 0 && count === $scope.Grid.Data.length) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = true;
                }
            };
            /* This function click refesh grid program */
            $scope.Grid.Refresh = function () {
                $scope.Grid.PageNumber = 1;
                $scope.Filter.Text = "";
                $location.search("ts", null);
                $scope.Selected.LinhVuc = null;
                $location.search("lv", null);
                $scope.Selected.ChucDanhNghienCuu = null;
                $location.search("cdnc", null);
                initData();
            };
            /* ------------------------------------------------------------------------------- */

            /* INIT FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            // Init nút chức năng theo quyền nhà khoa học
            var initButtonByRightOfNhaKhoaHoc = function () {
                $scope.Button.Add.GrantAccess = true;
                $scope.Button.Update.GrantAccess = true;
                $scope.Button.Delete.GrantAccess = true;
                $scope.Button.DownloadAttactment.GrantAccess = true;

                return true;
            };

            /* Load data from API binding to Grid*/
            var initData = function () {
                $scope.Grid.CheckAll = false;
                $scope.Button.Delete.Visible = false;
                $scope.Grid.Data.forEach(function (items) {
                    items.selected = null;
                });
                $scope.Grid.CheckAll = null;

                var qs = $location.search();
                /* PageNumber */
                if (typeof (qs["pn"]) !== 'undefined') {
                    $scope.Grid.PageNumber = parseInt(qs["pn"]);
                } else {
                    // 
                    $location.search("pn", "1");
                    $scope.Grid.PageNumber = 1;
                };
                /* PageSize */
                if (typeof (qs["ps"]) !== 'undefined') {
                    $scope.Grid.PageSize = parseInt(qs["ps"]);
                } else {
                    // 
                    $location.search("ps", "10");
                    $scope.Grid.PageSize = 10;
                };
                /* TextSearch */
                if (typeof (qs["ts"]) !== 'undefined') {
                    $scope.Filter.Text = qs["ts"];
                } else {
                    // 
                    $location.search("ts", "");
                    $scope.Filter.Text = "";
                };

                /* Linh Vuc */
                if (typeof (qs["lv"]) !== 'undefined') {
                    $scope.Selected.LinhVuc = qs["lv"];
                } else {
                    // 
                    $location.search("lv", null); $scope.Selected.LinhVuc = null;
                };

                /* chuc danh nghien cuu */
                if (typeof (qs["cdnc"]) !== 'undefined') {
                    $scope.Selected.ChucDanhNghienCuu = qs["cdnc"];
                } else {
                    $location.search("cdnc", null); $scope.Selected.ChucDanhNghienCuu = null;
                };



                var page = $scope.Grid.PageNumber;
                var size = $scope.Grid.PageSize;
                var filterObj = {
                    "fullTextSearch": $scope.Filter.Text,
                }
                if ($scope.Selected.ChucDanhNghienCuu) {
                    filterObj.chucDanhNghienCuu = $scope.Selected.ChucDanhNghienCuu
                }
                if ($scope.Selected.LinhVuc) {
                    filterObj.linhVuc = $scope.Selected.LinhVuc
                }

                var sort = "";

                var promise = NhaKhoaHocApiService.GetFilter(page, size, filterObj, sort);
                promise.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.code == 200) {
                        var dataResult = response.data;
                        $scope.Grid.Data = dataResult.data.content;
                        $scope.Grid.TotalCount = dataResult.data.totalElements;
                        $scope.Grid.TotalPage = dataResult.data.totalPages;
                        $scope.Grid.FromRecord = dataResult.data.size * (dataResult.data.page - 1);
                        $scope.Grid.ToRecord = $scope.Grid.FromRecord + dataResult.data.numberOfElements;
                    } else {
                        $log.error("ERROR", response);
                        UtilsService.OpenDialog('Kết quả', 'Thông báo: ' + response.data.message, '', 'Đóng', 'md');
                    }
                }, function onError(response) {
                    $log.error("ERROR", response);
                    UtilsService.OpenDialog('Kết quả', 'Thông báo: ' + response.data.message, '', 'Đóng', 'md');
                });
                return promise;
            };

            var initMain = function () {
                $q.all([initButtonByRightOfNhaKhoaHoc()]).then(function () {
                    initData();
                });
            };

            initMain();

            /* ------------------------------------------------------------------------------- */
        }
    ]);

    /* Controller for froms Popup Add,Edit,Delete or Info */
    app.controller("NhaKhoaHocModalController", ["$scope", "$q", "$uibModalInstance", '$log', 'Upload', "ConstantsApp", "NodeApiService", '$timeout',
        "item", "option", "toastr", "NhaKhoaHocApiService", "UtilsService", "RoleApiService",
        function ($scope, $q, $uibModalInstance, $log, Upload, ConstantsApp, NodeApiService, $timeout, item, option, $notifications, NhaKhoaHocApiService, UtilsService, RoleApiService) {
            /* ------------------------------------------------------------------------------- */
            /* SELECTED */
            /* ------------------------------------------------------------------------------- */
            $scope.Selected = {};
            $scope.Selected.LinhVucList = ConstantsApp.LinhVucList;
            $scope.Selected.ChucDanhNghienCuuList = ConstantsApp.ChucDanhNghienCuuList;
            $scope.Selected.HocHamList = ConstantsApp.HocHamList;
            $scope.Selected.HocViList = ConstantsApp.HocViList;
            $scope.Selected.NamSinhList = [];
            for (let index = (new Date()).getFullYear(); index >= 1500; index--) {
                $scope.Selected.NamSinhList.push({ Name: index });

            }
            $scope.Config = {
                "maxSize": "2GB",
                "multiple": true
            };
            $scope.Form = {};
            $scope.Form.Title = "";
            $scope.Form.CurrentUserId = app.CurrentUser.Id;
            $scope.Form.ApplicationId = app.CurrentUser.ApplicationId;
            $scope.Form.Item = angular.copy(item);
            $scope.Form.Option = option;
            $scope.Button = {};
            $scope.Button.Close = {};
            $scope.Button.Close.Click = function () {
                $uibModalInstance.dismiss('cancel');
            };

            $scope.Autofocus = false;

            /* Check parameters type = 'edit' to load data edit to form edit */

            /* Function button "Save" with add and edit button*/
            $scope.Save = function () {
                if (!$scope.form.$valid) {
                    angular.element("[name='" + $scope.form.$name + "']").find('.ng-invalid:visible:first').focus();
                    $notifications.warning("Vui lòng  kiểm tra thông tin đã nhập");
                    return false;
                }
                else {
                    $scope.Form.Item.LinhVuc = "";
                    for (let index = 0; index < $scope.Selected.LinhVucList.length; index++) {
                        const element = $scope.Selected.LinhVucList[index];
                        if (element.selected) {
                            if (index !== 0) {
                                $scope.Form.Item.LinhVuc += ", ";
                            }
                            $scope.Form.Item.LinhVuc += element.Name;
                        }
                    }
                    $scope.Form.Item.fileDinhKem = "";
                    $scope.Form.Item.fileDinhKem = angular.toJson($scope.Form.Item.fileDinhKemList);
                    if ($scope.Form.Option[0].Type === 'add') {
                        var postData = $scope.Form.Item;

                        var promise = NhaKhoaHocApiService.Create(postData);
                        promise.then(function onSuccess(response) {
                            var dataResult = response.data;
                            if (dataResult.code == 200) {
                                var dataResult = response.data;
                                $uibModalInstance.close(dataResult);
                            } else {
                                $log.error("ERROR", response);
                                UtilsService.OpenDialog('Kết quả', 'Chú ý', 'Thông báo : ' + response.data.message, 'Đóng', 'md');
                            }
                        }, function onError(response) {
                            $log.error("ERROR", response);
                            UtilsService.OpenDialog('Kết quả', 'Chú ý', 'Thông báo : ' + response.data.message, 'Đóng', 'md');
                        });
                        return promise;
                    } else {
                        var putData = $scope.Form.Item;
                        promise = NhaKhoaHocApiService.Update(putData.id, putData);
                        promise.then(function onSuccess(response) {
                            var dataResult = response.data;
                            if (dataResult.code == 200) {
                                var dataResult = response.data;
                                $uibModalInstance.close(dataResult);
                            } else {
                                $log.error("ERROR", response);
                                UtilsService.OpenDialog('Kết quả', 'Chú ý', 'Thông báo : ' + response.data.message, 'Đóng', 'md');
                            }
                        }, function onError(response) {
                            $log.error("ERROR", response);
                            UtilsService.OpenDialog('Kết quả', 'Chú ý', 'Thông báo : ' + response.data.message, 'Đóng', 'md');
                        });
                        return promise;
                    }
                }
            };


            $scope.UploadAttacment = function (files, file, newFiles, duplicateFiles, invalidFiles, event) {
                var urlUpload = NodeApiService.GetPhysicalUploadSrc("attactment", false, true);
                //upload multilfile 1 lúc
                var promise = Upload.upload({
                    url: urlUpload,
                    data: {
                        files: files,
                    }
                });
                promise.then(function (response) {
                    if (response.data != null) {
                        var dataResult = response.data;
                        if (dataResult.code === 200) {
                            if (!$scope.Form.Item.fileDinhKemList) {
                                $scope.Form.Item.fileDinhKemList = [];
                            }
                            for (let index = 0; index < dataResult.data.length; index++) {
                                const element = dataResult.data[index];
                                $scope.Form.Item.fileDinhKemList.push({
                                    name: element.name,
                                    path: element.physicalPath,
                                    size: element.size,
                                    extension: element.extension
                                });
                            }

                            $scope.Progress = null;
                            $scope.file = null;
                        }
                    }
                }, function (response) { }, function (evt) {
                    $scope.Progress = parseInt(100.0 * evt.loaded / evt.total);
                });
            };

            $scope.DownloadAttactment = function (att) {
                var uri = ConstantsApp.API_STATIC_FILE + att.path;
                var a = document.createElement('a');
                a.href = uri;
                a.download =att.name;
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
                
            };
            $scope.DeleteAttactment = function (att) {
              var index = $scope.Form.Item.fileDinhKemList.indexOf(att);
              if(index>=0){
                  $scope.Form.Item.fileDinhKemList.splice(index,1);
              }  
            };
            var initMain = function () {
                if ($scope.Form.Option[0].Type === 'edit') {
                    $scope.Form.Title = "Cập nhật nhà khoa học";
                    $scope.Form.Type = 1;
                    var currentLinhVucList = $scope.Form.Item.linhVuc.split(", ");
                    for (let i = 0; i < $scope.Selected.LinhVucList.length; i++) {
                        const item = $scope.Selected.LinhVucList[i];
                        for (let j = 0; j < currentLinhVucList.length; j++) {
                            const currentItem = currentLinhVucList[j];
                            if (item.Name === currentItem) {
                                item.selected = true;
                            }
                        }
                    }

                    $scope.Form.Item.fileDinhKemList = {};
                    try {
                        $scope.Form.Item.fileDinhKemList = angular.fromJson($scope.Form.Item.fileDinhKem);
                    } catch (error) {
                        $scope.Form.Item.fileDinhKemList = [];
                    }

                } /* Check parameters type = 'info' to load data info to form info */
                else if ($scope.Form.Option[0].Type === 'add') {
                    $scope.Form.Title = "Thêm nhà khoa học";
                    $scope.Form.Type = 0;
                    $scope.Form.Item = {};
                    $scope.Form.Item.gioiTinh = 1;
                    $scope.Form.Item.namSinh = (new Date()).getFullYear();

                };
            };

            initMain();
        }
    ]);
}();