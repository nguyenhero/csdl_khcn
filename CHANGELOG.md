# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/SAVIS-GIT/CSDL_KHCNcompare/v2.0.16...v1.0.0) (2018-09-19)
