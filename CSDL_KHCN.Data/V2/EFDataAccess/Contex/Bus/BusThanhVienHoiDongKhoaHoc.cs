namespace CSDL_KHCN.Data_V2
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("bus_Thanh_Vien_Hoi_Dong_Khoa_Hoc")]
    public class BusThanhVienHoiDongKhoaHoc  
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long? Id { get; set; }
        [ForeignKey("HoiDongKhoaHoc")]
        public Guid HoiDongKhoaHocId { get; set; }
        [ForeignKey("NhaKhoaHoc")]
        public Guid NhaKhoaHocId { get; set; }
        public string ChucVu { get; set; }
        public BusNhaKhoaHoc NhaKhoaHoc { get; set; }
        public BusHoiDongKhoaHoc HoiDongKhoaHoc { get; set; }

    }
}
