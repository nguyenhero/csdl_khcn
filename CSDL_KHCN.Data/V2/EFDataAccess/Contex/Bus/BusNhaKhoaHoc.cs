namespace CSDL_KHCN.Data_V2
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    using System.Collections.Generic;

    [Table("bus_Nha_Khoa_Hoc")]
    public  class BusNhaKhoaHoc : BaseTable<BusNhaKhoaHoc>
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string HoTen { get; set; }
        public int NamSinh { get; set; } 
        public int GioiTinh { get; set; } 
        public string HocHam { get; set; } 
        public string HocVi { get; set; } 
        public string LinhVuc { get; set; } 
        public string ChucDanhNghienCuu { get; set; } 
        public string ChucVuHienNay { get; set; } 
        public string DienThoaiNhaRieng { get; set; } 
        public string Mobile { get; set; } 
        public string Email { get; set; } 
        public string TenCoQuan { get; set; } 
        public string TenNguoiDungDauCoQuan { get; set; } 
        public string DiaChiCoQuan { get; set; } 
        public string DienThoaiCoQuan { get; set; } 
        public string FaxCoQuan { get; set; } 
        public string WebsiteCoQuan { get; set; } 
        public string FileDinhKem { get; set; }  
    }
}
