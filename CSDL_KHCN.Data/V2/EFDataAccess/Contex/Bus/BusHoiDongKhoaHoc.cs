namespace CSDL_KHCN.Data_V2
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Collections.Generic;

    [Table("bus_Hoi_Dong_Khoa_Hoc")]
    public class BusHoiDongKhoaHoc : BaseTable<BusHoiDongKhoaHoc>
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Ten { get; set; }
        [Required]
        public string Ma { get; set; }
        public int NamThanhLap { get; set; }
        public int TrangThai { get; set; }
        public string FileDinhKem { get; set; } 
    }
}
