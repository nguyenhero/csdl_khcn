﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSDL_KHCN.Data_V2
{

    [Table("sys_Appliation")]
    public  class SysApplication:BaseTableDefault
    {
        public SysApplication()
        { }
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(128)]
        public string Name { get; set; } 
        [StringLength(1024)]
        public string Description { get; set; }
        [Required]
        [StringLength(64)]
        public string Code { get; set; }
    }
}
