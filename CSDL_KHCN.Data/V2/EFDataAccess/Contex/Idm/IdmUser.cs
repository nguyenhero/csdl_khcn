﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSDL_KHCN.Data_V2
{
    [Table("idm_User")]
    public  class IdmUser:BaseTableDefault
    {
        public IdmUser()
        {
            IdmUserMapRole = new HashSet<IdmUserMapRole>();
        }

        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(128)]
        public string UserName { get; set; }
        /*Attribute */
        [Required]
        [StringLength(128)]
        public string Name { get; set; }
        [StringLength(256)]
        public string PhoneNumber { get; set; }
        [StringLength(256)]
        public string Email { get; set; }
        [StringLength(1024)]
        public string AvatarUrl { get; set; }
        public int? Type { get; set; }
        [StringLength(1024)]
        public string Password { get; set; }
        [StringLength(1024)]
        public string PasswordSalt { get; set; }
        public DateTime? Birthdate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public ICollection<IdmUserMapRole> IdmUserMapRole { get; set; }
    }
}
