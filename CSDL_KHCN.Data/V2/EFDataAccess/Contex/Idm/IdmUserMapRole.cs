﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSDL_KHCN.Data_V2
{
    [Table("idm_User_Map_Role")]
    public  class IdmUserMapRole :BaseTable<IdmUserMapRole>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Column(Order = 1) , ForeignKey("User")]
        public Guid UserId { get; set; }
        [Column(Order = 2) , ForeignKey("Application")]
        public override Guid ApplicationId { get; set; }
        [Column(Order = 3),   ForeignKey("Role")]
        public Guid RoleId { get; set; }
        public virtual IdmRole Role { get; set; }
        public virtual IdmUser User { get; set; }
    }
}
