﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSDL_KHCN.Data_V2
{
    [Table("idm_Role")]
    public  class IdmRole:BaseTableDefault
    {
        public IdmRole()
        {
            IdmUserMapRole = new HashSet<IdmUserMapRole>();
        }

        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(128)]
        public string Name { get; set; }
        [Required]
        [StringLength(64)]
        public string Code { get; set; }
        [StringLength(1024)]
        public string Description { get; set; }
        public ICollection<IdmUserMapRole> IdmUserMapRole { get; set; }
    }
}
