﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Data.SqlClient;

namespace CSDL_KHCN.Data_V2
{
    public static class ConnectionTools
    {
        public static void ChangeDatabaseConnection(
                this DbContext source,
                string connectionString)
        /* this would be used if the
        *  connectionString name varied from 
        *  the base EF class name */
        {
            try
            {
                // now flip the properties that were changed
                source.Database.GetDbConnection().ConnectionString
                    = connectionString;
            }
            catch (Exception)
            {
                // set log item if required
            }
        }
    }

    public class DatabaseFactory : IDatabaseFactory
    {
        private readonly DbContext _dataContext;
        public string Prefix;

        public DatabaseFactory(string prefix = "", string connectionstring = "")
        {


            Prefix = prefix;

            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            if (string.IsNullOrEmpty(connectionstring))
            {
                connectionstring = Utils.GetConfig("ConnectionString:" + databaseType);
            }
            _dataContext = new DataContext(prefix, databaseType, connectionstring, isTesting);


            // Get randomize Id
            var random = new Random(DateTime.Now.Millisecond);
            Id = random.Next(1000000).ToString();
        }

        public string Id { get; set; }

        public DbContext GetDbContext()
        {
            return _dataContext;
        }

        public string GetPrefix()
        {
            return Prefix;
        }
    }
}