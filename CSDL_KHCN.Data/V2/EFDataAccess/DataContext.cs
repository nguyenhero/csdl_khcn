﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace CSDL_KHCN.Data_V2
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {

            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            return new DataContext("_1", databaseType, connectionString, isTesting);
        }
    }
    public class DataContext : DbContext
    {

        #region sys
        public virtual DbSet<SysApplication> SysApplication { get; set; }
        #endregion

        #region bsd
        public DbSet<BsdParameter> BsdParameter { get; set; }
        #endregion
        #region idm
        public virtual DbSet<IdmRole> IdmRole { get; set; }
        public virtual DbSet<IdmUser> IdmUser { get; set; }
        public virtual DbSet<IdmUserMapRole> IdmUserMapRole { get; set; }
        #endregion 
        #region bus
        public virtual DbSet<BusNhaKhoaHoc> BusNhaKhoaHoc { get; set; }
        public virtual DbSet<BusThanhVienHoiDongKhoaHoc> BusThanhVienHoiDongKhoaHoc { get; set; }
        public virtual DbSet<BusHoiDongKhoaHoc> BusHoiDongKhoaHoc { get; set; }

        #endregion
        public DataContext(string prefix, string databaseType, string connectionString, bool isTesting = false)
        {
            Prefix = prefix;
            DatabaseType = databaseType;
            ConnectionString = connectionString;
            IsTesting = isTesting;
        }
        public string Prefix { get; set; }
        public string DatabaseType { get; set; }
        public string ConnectionString { get; set; }
        public bool IsTesting { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            if (!optionsBuilder.IsConfigured)
            {
                #region Config database
                if (IsTesting)
                {
                    optionsBuilder.UseInMemoryDatabase("testDatabase");
                }
                else
                {
                    switch (DatabaseType)
                    {
                        case "MySqlPomeloDatabase":
                            optionsBuilder.UseMySql(ConnectionString);
                            break;
                        case "MSSQLDatabase":
                            optionsBuilder.UseSqlServer(ConnectionString);
                            break;
                        case "OracleDatabase":
                            optionsBuilder.UseOracle(ConnectionString);
                            break;
                        case "PostgreSQLDatabase":
                            optionsBuilder.UseNpgsql(ConnectionString);
                            break;
                        case "Sqlite":
                            optionsBuilder.UseSqlite(ConnectionString);
                            break;
                        default:
                            optionsBuilder.UseSqlServer(ConnectionString);
                            break;
                    }
                }
                #endregion

                optionsBuilder.ReplaceService<IModelCacheKeyFactory, DynamicModelCacheKeyFactory>();
            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.HasDefaultSchema("CSDL_KHCN");
            // foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            //     relationship.DeleteBehavior = DeleteBehavior.Restrict;
            #region seed
            modelBuilder.Entity<SysApplication>().HasData(
                new SysApplication
                {
                    Id = AppConstants.RootAppId,
                    Name = "CSDL_KHCN",
                    Code = "1",
                    Description = "Giải pháp quản lý dữ liệu tập trung"
                },
                new SysApplication
                {
                    Id = AppConstants.TestAppId,
                    Name = "CSDL_KHCN2",
                    Code = "2",
                    Description = "Giải pháp quản lý dữ liệu tập trung"
                });

            modelBuilder.Entity<IdmUser>().HasData(
                new IdmUser
                {
                    Name = "Administrator",
                    UserName = "administrator",
                    Id = UserConstants.AdministratorId,
                    Type = 0,
                    LastActivityDate = DateTime.Now,
                    AvatarUrl = "https://avatars0.githubusercontent.com/u/18421313?v=4",
                    Email = "mri.hannibal@gmail.com",
                    PhoneNumber = "0967267469",
                    Password = "",
                    PasswordSalt = ""
                },
                new IdmUser
                {
                    Name = "GUEST",
                    UserName = "guest",
                    Id = UserConstants.UserId,
                    Type = 0,
                    LastActivityDate = DateTime.Now
                });

            modelBuilder.Entity<IdmRole>().HasData(
                new IdmRole
                {
                    Id = RoleConstants.AdministratorId,
                    Code = "SYS_ADMIN",
                    Name = "Quản trị hệ thống",
                    Description = ""
                },
                new IdmRole
                {
                    Id = RoleConstants.UserId,
                    Code = "USER",
                    Name = "Người dùng hệ thống",
                    Description = ""
                });



            modelBuilder.Entity<IdmUserMapRole>().HasData(
                new IdmUserMapRole
                {
                    Id = 1,
                    UserId = UserConstants.AdministratorId,
                    RoleId = RoleConstants.AdministratorId
                }.InitCreate(AppConstants.RootAppId, null),
                new IdmUserMapRole
                {
                    Id = 2,
                    UserId = UserConstants.UserId,
                    RoleId = RoleConstants.UserId
                }.InitCreate(AppConstants.RootAppId, null));
            #endregion
        }
    }

    public class DynamicModelCacheKeyFactory : IModelCacheKeyFactory
    {
        public object Create(DbContext context)
        {

            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            if (context is DataContext dynamicContext)
            {
                return (context.GetType(), dynamicContext.Prefix, databaseType, connectionString, isTesting);
            }

            return context.GetType();
        }
    }
}