﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CSDL_KHCN.Data.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "CSDL_KHCN");

            migrationBuilder.CreateTable(
                name: "idm_Role",
                schema: "CSDL_KHCN",
                columns: table => new
                {
                    LastModifiedOnDate = table.Column<DateTime>(nullable: true),
                    CreatedOnDate = table.Column<DateTime>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Code = table.Column<string>(maxLength: 64, nullable: false),
                    Description = table.Column<string>(maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "idm_User",
                schema: "CSDL_KHCN",
                columns: table => new
                {
                    LastModifiedOnDate = table.Column<DateTime>(nullable: true),
                    CreatedOnDate = table.Column<DateTime>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    AvatarUrl = table.Column<string>(maxLength: 1024, nullable: true),
                    Type = table.Column<int>(nullable: true),
                    Password = table.Column<string>(maxLength: 1024, nullable: true),
                    PasswordSalt = table.Column<string>(maxLength: 1024, nullable: true),
                    Birthdate = table.Column<DateTime>(nullable: true),
                    LastActivityDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_Appliation",
                schema: "CSDL_KHCN",
                columns: table => new
                {
                    LastModifiedOnDate = table.Column<DateTime>(nullable: true),
                    CreatedOnDate = table.Column<DateTime>(nullable: true),
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Description = table.Column<string>(maxLength: 1024, nullable: true),
                    Code = table.Column<string>(maxLength: 64, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_Appliation", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "bsd_Parameter",
                schema: "CSDL_KHCN",
                columns: table => new
                {
                    CreatedByUserId = table.Column<Guid>(nullable: false),
                    LastModifiedByUserId = table.Column<Guid>(nullable: false),
                    LastModifiedOnDate = table.Column<DateTime>(nullable: false),
                    CreatedOnDate = table.Column<DateTime>(nullable: false),
                    ApplicationId = table.Column<Guid>(nullable: false),
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 64, nullable: false),
                    Description = table.Column<string>(maxLength: 1024, nullable: true),
                    Value = table.Column<string>(maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_Parameter", x => x.Id);
                    table.ForeignKey(
                        name: "FK_bsd_Parameter_sys_Appliation_ApplicationId",
                        column: x => x.ApplicationId,
                        principalSchema: "CSDL_KHCN",
                        principalTable: "sys_Appliation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "bus_Hoi_Dong_Khoa_Hoc",
                schema: "CSDL_KHCN",
                columns: table => new
                {
                    CreatedByUserId = table.Column<Guid>(nullable: false),
                    LastModifiedByUserId = table.Column<Guid>(nullable: false),
                    LastModifiedOnDate = table.Column<DateTime>(nullable: false),
                    CreatedOnDate = table.Column<DateTime>(nullable: false),
                    ApplicationId = table.Column<Guid>(nullable: false),
                    Id = table.Column<Guid>(nullable: false),
                    Ten = table.Column<string>(nullable: false),
                    Ma = table.Column<string>(nullable: false),
                    NamThanhLap = table.Column<int>(nullable: false),
                    TrangThai = table.Column<int>(nullable: false),
                    FileDinhKem = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bus_Hoi_Dong_Khoa_Hoc", x => x.Id);
                    table.ForeignKey(
                        name: "FK_bus_Hoi_Dong_Khoa_Hoc_sys_Appliation_ApplicationId",
                        column: x => x.ApplicationId,
                        principalSchema: "CSDL_KHCN",
                        principalTable: "sys_Appliation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "bus_Nha_Khoa_Hoc",
                schema: "CSDL_KHCN",
                columns: table => new
                {
                    CreatedByUserId = table.Column<Guid>(nullable: false),
                    LastModifiedByUserId = table.Column<Guid>(nullable: false),
                    LastModifiedOnDate = table.Column<DateTime>(nullable: false),
                    CreatedOnDate = table.Column<DateTime>(nullable: false),
                    ApplicationId = table.Column<Guid>(nullable: false),
                    Id = table.Column<Guid>(nullable: false),
                    HoTen = table.Column<string>(nullable: false),
                    NamSinh = table.Column<int>(nullable: false),
                    GioiTinh = table.Column<int>(nullable: false),
                    HocHam = table.Column<string>(nullable: true),
                    HocVi = table.Column<string>(nullable: true),
                    LinhVuc = table.Column<string>(nullable: true),
                    ChucDanhNghienCuu = table.Column<string>(nullable: true),
                    ChucVuHienNay = table.Column<string>(nullable: true),
                    DienThoaiNhaRieng = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    TenCoQuan = table.Column<string>(nullable: true),
                    TenNguoiDungDauCoQuan = table.Column<string>(nullable: true),
                    DiaChiCoQuan = table.Column<string>(nullable: true),
                    DienThoaiCoQuan = table.Column<string>(nullable: true),
                    FaxCoQuan = table.Column<string>(nullable: true),
                    WebsiteCoQuan = table.Column<string>(nullable: true),
                    FileDinhKem = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bus_Nha_Khoa_Hoc", x => x.Id);
                    table.ForeignKey(
                        name: "FK_bus_Nha_Khoa_Hoc_sys_Appliation_ApplicationId",
                        column: x => x.ApplicationId,
                        principalSchema: "CSDL_KHCN",
                        principalTable: "sys_Appliation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "idm_User_Map_Role",
                schema: "CSDL_KHCN",
                columns: table => new
                {
                    CreatedByUserId = table.Column<Guid>(nullable: false),
                    LastModifiedByUserId = table.Column<Guid>(nullable: false),
                    LastModifiedOnDate = table.Column<DateTime>(nullable: false),
                    CreatedOnDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<Guid>(nullable: false),
                    ApplicationId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_User_Map_Role", x => x.Id);
                    table.ForeignKey(
                        name: "FK_idm_User_Map_Role_sys_Appliation_ApplicationId",
                        column: x => x.ApplicationId,
                        principalSchema: "CSDL_KHCN",
                        principalTable: "sys_Appliation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_idm_User_Map_Role_idm_Role_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "CSDL_KHCN",
                        principalTable: "idm_Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_idm_User_Map_Role_idm_User_UserId",
                        column: x => x.UserId,
                        principalSchema: "CSDL_KHCN",
                        principalTable: "idm_User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "bus_Thanh_Vien_Hoi_Dong_Khoa_Hoc",
                schema: "CSDL_KHCN",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    HoiDongKhoaHocId = table.Column<Guid>(nullable: false),
                    NhaKhoaHocId = table.Column<Guid>(nullable: false),
                    ChucVu = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bus_Thanh_Vien_Hoi_Dong_Khoa_Hoc", x => x.Id);
                    table.ForeignKey(
                        name: "FK_bus_Thanh_Vien_Hoi_Dong_Khoa_Hoc_bus_Hoi_Dong_Khoa_Hoc_HoiDongKhoaHocId",
                        column: x => x.HoiDongKhoaHocId,
                        principalSchema: "CSDL_KHCN",
                        principalTable: "bus_Hoi_Dong_Khoa_Hoc",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_bus_Thanh_Vien_Hoi_Dong_Khoa_Hoc_bus_Nha_Khoa_Hoc_NhaKhoaHocId",
                        column: x => x.NhaKhoaHocId,
                        principalSchema: "CSDL_KHCN",
                        principalTable: "bus_Nha_Khoa_Hoc",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "CSDL_KHCN",
                table: "idm_Role",
                columns: new[] { "Id", "Code", "CreatedOnDate", "Description", "LastModifiedOnDate", "Name" },
                values: new object[] { new Guid("00000000-0000-0000-0000-000000000001"), "SYS_ADMIN", new DateTime(2018, 11, 23, 17, 12, 5, 679, DateTimeKind.Local), "", new DateTime(2018, 11, 23, 17, 12, 5, 679, DateTimeKind.Local), "Quản trị hệ thống" });

            migrationBuilder.InsertData(
                schema: "CSDL_KHCN",
                table: "idm_Role",
                columns: new[] { "Id", "Code", "CreatedOnDate", "Description", "LastModifiedOnDate", "Name" },
                values: new object[] { new Guid("00000000-0000-0000-0000-000000000002"), "USER", new DateTime(2018, 11, 23, 17, 12, 5, 679, DateTimeKind.Local), "", new DateTime(2018, 11, 23, 17, 12, 5, 679, DateTimeKind.Local), "Người dùng hệ thống" });

            migrationBuilder.InsertData(
                schema: "CSDL_KHCN",
                table: "idm_User",
                columns: new[] { "Id", "AvatarUrl", "Birthdate", "CreatedOnDate", "Email", "LastActivityDate", "LastModifiedOnDate", "Name", "Password", "PasswordSalt", "PhoneNumber", "Type", "UserName" },
                values: new object[] { new Guid("00000000-0000-0000-0000-000000000001"), "https://avatars0.githubusercontent.com/u/18421313?v=4", null, new DateTime(2018, 11, 23, 17, 12, 5, 678, DateTimeKind.Local), "mri.hannibal@gmail.com", new DateTime(2018, 11, 23, 17, 12, 5, 679, DateTimeKind.Local), new DateTime(2018, 11, 23, 17, 12, 5, 678, DateTimeKind.Local), "Administrator", "", "", "0967267469", 0, "administrator" });

            migrationBuilder.InsertData(
                schema: "CSDL_KHCN",
                table: "idm_User",
                columns: new[] { "Id", "AvatarUrl", "Birthdate", "CreatedOnDate", "Email", "LastActivityDate", "LastModifiedOnDate", "Name", "Password", "PasswordSalt", "PhoneNumber", "Type", "UserName" },
                values: new object[] { new Guid("00000000-0000-0000-0000-000000000002"), null, null, new DateTime(2018, 11, 23, 17, 12, 5, 679, DateTimeKind.Local), null, new DateTime(2018, 11, 23, 17, 12, 5, 679, DateTimeKind.Local), new DateTime(2018, 11, 23, 17, 12, 5, 679, DateTimeKind.Local), "GUEST", null, null, null, 0, "guest" });

            migrationBuilder.InsertData(
                schema: "CSDL_KHCN",
                table: "sys_Appliation",
                columns: new[] { "Id", "Code", "CreatedOnDate", "Description", "LastModifiedOnDate", "Name" },
                values: new object[] { new Guid("00000000-0000-0000-0000-000000000001"), "1", new DateTime(2018, 11, 23, 17, 12, 5, 678, DateTimeKind.Local), "Giải pháp quản lý dữ liệu tập trung", new DateTime(2018, 11, 23, 17, 12, 5, 677, DateTimeKind.Local), "CSDL_KHCN" });

            migrationBuilder.InsertData(
                schema: "CSDL_KHCN",
                table: "sys_Appliation",
                columns: new[] { "Id", "Code", "CreatedOnDate", "Description", "LastModifiedOnDate", "Name" },
                values: new object[] { new Guid("00000000-0000-0000-0000-000000000002"), "2", new DateTime(2018, 11, 23, 17, 12, 5, 678, DateTimeKind.Local), "Giải pháp quản lý dữ liệu tập trung", new DateTime(2018, 11, 23, 17, 12, 5, 678, DateTimeKind.Local), "CSDL_KHCN2" });

            migrationBuilder.InsertData(
                schema: "CSDL_KHCN",
                table: "idm_User_Map_Role",
                columns: new[] { "Id", "ApplicationId", "CreatedByUserId", "CreatedOnDate", "LastModifiedByUserId", "LastModifiedOnDate", "RoleId", "UserId" },
                values: new object[] { 1L, new Guid("00000000-0000-0000-0000-000000000001"), new Guid("00000000-0000-0000-0000-000000000001"), new DateTime(2018, 11, 23, 17, 12, 5, 680, DateTimeKind.Local), new Guid("00000000-0000-0000-0000-000000000001"), new DateTime(2018, 11, 23, 17, 12, 5, 680, DateTimeKind.Local), new Guid("00000000-0000-0000-0000-000000000001"), new Guid("00000000-0000-0000-0000-000000000001") });

            migrationBuilder.InsertData(
                schema: "CSDL_KHCN",
                table: "idm_User_Map_Role",
                columns: new[] { "Id", "ApplicationId", "CreatedByUserId", "CreatedOnDate", "LastModifiedByUserId", "LastModifiedOnDate", "RoleId", "UserId" },
                values: new object[] { 2L, new Guid("00000000-0000-0000-0000-000000000001"), new Guid("00000000-0000-0000-0000-000000000001"), new DateTime(2018, 11, 23, 17, 12, 5, 680, DateTimeKind.Local), new Guid("00000000-0000-0000-0000-000000000001"), new DateTime(2018, 11, 23, 17, 12, 5, 680, DateTimeKind.Local), new Guid("00000000-0000-0000-0000-000000000002"), new Guid("00000000-0000-0000-0000-000000000002") });

            migrationBuilder.CreateIndex(
                name: "IX_bsd_Parameter_ApplicationId",
                schema: "CSDL_KHCN",
                table: "bsd_Parameter",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_bus_Hoi_Dong_Khoa_Hoc_ApplicationId",
                schema: "CSDL_KHCN",
                table: "bus_Hoi_Dong_Khoa_Hoc",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_bus_Nha_Khoa_Hoc_ApplicationId",
                schema: "CSDL_KHCN",
                table: "bus_Nha_Khoa_Hoc",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_bus_Thanh_Vien_Hoi_Dong_Khoa_Hoc_HoiDongKhoaHocId",
                schema: "CSDL_KHCN",
                table: "bus_Thanh_Vien_Hoi_Dong_Khoa_Hoc",
                column: "HoiDongKhoaHocId");

            migrationBuilder.CreateIndex(
                name: "IX_bus_Thanh_Vien_Hoi_Dong_Khoa_Hoc_NhaKhoaHocId",
                schema: "CSDL_KHCN",
                table: "bus_Thanh_Vien_Hoi_Dong_Khoa_Hoc",
                column: "NhaKhoaHocId");

            migrationBuilder.CreateIndex(
                name: "IX_idm_User_Map_Role_ApplicationId",
                schema: "CSDL_KHCN",
                table: "idm_User_Map_Role",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_idm_User_Map_Role_RoleId",
                schema: "CSDL_KHCN",
                table: "idm_User_Map_Role",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_idm_User_Map_Role_UserId",
                schema: "CSDL_KHCN",
                table: "idm_User_Map_Role",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "bsd_Parameter",
                schema: "CSDL_KHCN");

            migrationBuilder.DropTable(
                name: "bus_Thanh_Vien_Hoi_Dong_Khoa_Hoc",
                schema: "CSDL_KHCN");

            migrationBuilder.DropTable(
                name: "idm_User_Map_Role",
                schema: "CSDL_KHCN");

            migrationBuilder.DropTable(
                name: "bus_Hoi_Dong_Khoa_Hoc",
                schema: "CSDL_KHCN");

            migrationBuilder.DropTable(
                name: "bus_Nha_Khoa_Hoc",
                schema: "CSDL_KHCN");

            migrationBuilder.DropTable(
                name: "idm_Role",
                schema: "CSDL_KHCN");

            migrationBuilder.DropTable(
                name: "idm_User",
                schema: "CSDL_KHCN");

            migrationBuilder.DropTable(
                name: "sys_Appliation",
                schema: "CSDL_KHCN");
        }
    }
}
